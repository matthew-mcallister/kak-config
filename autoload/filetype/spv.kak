# https://github.com/KhronosGroup/SPIRV-Tools
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Detection
# ~~~~~~~~~

hook global BufCreate .*\.spvasm %{
    set-option buffer filetype spv
}

# Highlighters
# ~~~~~~~~~~~~

add-highlighter shared/spv regions
add-highlighter shared/spv/code default-region group
add-highlighter shared/spv/comment region %{;} $ group
add-highlighter shared/spv/string region %{"} %{(?<!\\)(\\\\)*"} fill string

add-highlighter shared/spv/comment/ fill comment
add-highlighter shared/spv/comment/ regex \b(?:TODO|XXX|FIXME)\b 0:default+r

# Variables
add-highlighter shared/spv/code/ regex '%[_\w\d]+\b' 0:value

# Literals and operators
add-highlighter shared/spv/code/ regex \b(?:(?:[1-9][0-9]*|0[xX][0-9a-fA-F]+|0[0-7]*)[uU]?|(?:[0-9]+[eE][+-]?[0-9]+|(?:[0-9]+\.|[0-9]*\.[0-9]+)(?:[eE][+-]?[0-9]+)?)(?:lf|LF|f|F)?) 0:value

# Commands
# ~~~~~~~~

define-command -hidden spv-remove-trailing-whitespace %{
    # remove trailing white spaces
    try %{ execute-keys -draft -itersel <a-x> s \h+$ <ret> d }
}

define-command -hidden spv-indent-on-new-line %~
    evaluate-commands -draft -itersel %<
        # copy // comments prefix and following white spaces
        try %{ execute-keys -draft k <a-x> s ^\h*\K//[!/]?\h* <ret> y gh j P }
        # preserve previous line indent
        try %{ execute-keys -draft \; K <a-&> }
        # filter previous line
        try %{ execute-keys -draft k : spv-filter-around-selections <ret> }
    >
~

# Initialization
# ~~~~~~~~~~~~~~

hook -group spv-highlight global WinSetOption filetype=spv %{
    add-highlighter window/spv ref spv
    hook -once -always window WinSetOption filetype=.* %{ remove-highlighter window/spv }
}

hook global WinSetOption filetype=spv %[
    hook window InsertChar \n -group spv-indent spv-indent-on-new-line
    hook window BufWritePre %val{buffile} -group spv-hooks spv-remove-trailing-whitespace
    hook -once -always window WinSetOption filetype=.* %{ remove-hooks window spv-.+ }
]
