# x86 assembly
# ------------

# File detection
# --------------

# .s file extension
hook global BufCreate .*\.ll$ %{
    set-option buffer filetype llvm
}

# Hook commands
# -------------

define-command -hidden llvm-indent-on-new-line %<
    evaluate-commands -draft -itersel %<
        execute-keys \;
        # Copy ; comment prefix
        try %< execute-keys -draft k x s ^\h*;\h* <ret> y j gh P >
        # Preserve previous line indent
        try %< execute-keys -draft K <a-&> >
        # Indent after [{[(:]
        try %< execute-keys -draft k x <a-k> [{(\[:]\h*$ <ret> j <a-gt> >
    >
>

define-command -hidden llvm-remove-comment-markers %<
    evaluate-commands -draft -itersel %<
        # Remove `;` prefix when joining one comment line to the next
        try %< execute-keys -draft <a-l> s \A\h*;\h* <ret> c ' ' >
    >
>

define-command -hidden llvm-remove-trailing-whitespace %[
    evaluate-commands -draft %_
        # Remove trailing whitespace on all lines
        try %[ execute-keys -draft \% s \h+$ <ret> d ]
    _
]

# Syntax highlighting
# -------------------

add-highlighter shared/llvm regions
add-highlighter shared/llvm/code default-region group
add-highlighter shared/llvm/string-ident region %{[@%](?<!')(?<!'\\)"} %{(?<!\\)(?:\\\\)*"} fill string
add-highlighter shared/llvm/string region %{(?<![@%])c?(?<!')(?<!'\\)"} %{(?<!\\)(?:\\\\)*"} fill string
add-highlighter shared/llvm/line_comment region ';' '$' group

add-highlighter shared/llvm/line_comment/ fill comment
add-highlighter shared/llvm/line_comment/ regex \b(?:TODO|XXX|FIXME)\b 0:default+r

# Operators
add-highlighter shared/llvm/code/ regex [=&|?+\-/%!~*\^#<>] 0:operator

# Numeric literals
add-highlighter shared/llvm/code/ regex %{\b[1-9]('?\d+)*\b} 0:value
add-highlighter shared/llvm/code/ regex %{\b0('?[0-7]+)*\b} 0:value
add-highlighter shared/llvm/code/ regex %{(?i)\b0x[\da-f]('?[\da-f]+)*\b} 0:value
add-highlighter shared/llvm/code/ regex [0-9][_0-9]*(?:\.[0-9][_0-9]*)(?:[eE][\+\-]?[_0-9]+) 0:value

# Labels
add-highlighter shared/llvm/code/ regex ^\h*([A-Za-z0-9_\-.]+): 1:function
# Identifiers
add-highlighter shared/llvm/code/ regex [%@][a-zA-Z0-9_\-.]+ 0:string

# Integer types
add-highlighter shared/llvm/code/ regex \bi\d+\b 0:type
# Other types
add-highlighter shared/llvm/code/ regex \b(:?void|float|double|ppc_fp128|fp128|x86_fp80)\b 0:type

# Keywords
add-highlighter shared/llvm/code/ regex \b(to|define|declare|module|asm|type|triple|datalayout|begin|target|deplibs|source_filename|attributes|distinct|llvm\.module\.flags)\b 0:keyword
# Operators
add-highlighter shared/llvm/code/ regex \b(?:add|fadd|sub|fsub|mul|fmul|udiv|sdiv|fdiv|urem|srem|frem|neg|fneg|shl|lshr|ashr|and|or|xor|trunc|zext|sext|fptrunc|fpext|bitcast|uitofp|sitofp|fptoui|fptosi|inttoptr|ptrtoint|eq|ne|slt|sgt|sle|sge|ult|ugt|ule|ugt|oeq|one|olt|ogt|ole|oge|ord|uno|ueq|une|ult|ugt|ule|uge|addrspace|ret|br|label|switch|invoke|unwind|unreachable|icmp|fcmp|getelementptr|select|va_arg|extractelement|insertelement|shufflevector|extractvalue|insertvalue|malloc|alloca|free|getresult|call|tail|load|store|phi)\b 0:keyword
# Attributes
add-highlighter shared/llvm/code/ regex \b(?:internal|weak|weak_odr|linkonce|linkonce_odr|appending|dllexport|common|private|linker_private|linker_private_weak|dllimport|extern_weak|external|default|hidden|protected|dllimport|ccc|fastcc|coldcc|x86_stdcallcc|x86_fastcallcc|cc|zeroext|signext|inreg|sret|noalias|nocapture|byval|nest|align|nounwind|readnone|readonly|inlinehint|alignstack|noinline|alwaysinline|optsize|ssp|sspreq|global|constant|thread_local|sideeffect|exact|volatile|nsw|nuw|inbounds|unnamed_addr|dereferenceable|dereferenceable_or_null|personality|nonnull|srcloc|range|alias\.scope|noreturn|nonlazybind|uwtable|cold|norecurse|willreturn|argmemonly|speculatable|local_unnamed_addr|writeonly|immarg|nofree)\b 0:attribute
# Constants
add-highlighter shared/llvm/code/ regex \b(?:true|false|null|undef|zeroinitializer)\b 0:value

# Vector syntax
add-highlighter shared/llvm/code/ regex \b\d+\b\h+(x) 1:operator

# Initialization
# ~~~~~~~~~~~~~~

hook -group llvm-highlight global WinSetOption filetype=llvm %{
    add-highlighter window/llvm ref llvm
    hook -once -always window WinSetOption filetype=.* %{ remove-highlighter window/a
sm}
}

hook global WinSetOption filetype=llvm %[
    hook window InsertChar \n -group llvm-hooks llvm-indent-on-new-line
    hook window NormalKey <a-j> -group llvm-hooks llvm-remove-comment-markers
    hook window NormalKey <a-J> -group llvm-hooks llvm-remove-comment-markers
    # TODO: %val{buffile} needs to be escaped to use as a regex
    hook window BufWritePre %val{buffile} -group llvm-hooks llvm-remove-trailing-whitespace
    hook -once -always window WinSetOption filetype=.* %{ remove-hooks windowllvm-.+ }
]
