# http://terraform-lang.org
# ~~~~~~~~~~~~~~~~~~~~

# Detection
# ~~~~~~~~~

hook global BufCreate .*\.(?:tf|tfvars) %{
    set-option buffer filetype terraform
}

# Highlighters
# ~~~~~~~~~~~~

add-highlighter shared/terraform regions
add-highlighter shared/terraform/code default-region group

add-highlighter shared/terraform/string region %{(?<!')b?r?"} %{(?<!\\)(\\\\)*"} fill string
add-highlighter shared/terraform/raw_string region %{r#"} %{"#} fill string

add-highlighter shared/terraform/line_comment region (//|#) $ group
add-highlighter shared/terraform/line_comment/ regex \b(?:TODO|XXX|FIXME)\b 0:default+r
add-highlighter shared/terraform/line_comment/ fill comment

add-highlighter shared/terraform/nested_comment region -recurse /\* /\* \*/ group
add-highlighter shared/terraform/nested_comment/ regex \b(?:TODO|XXX|FIXME)\b 0:default+r
add-highlighter shared/terraform/nested_comment/ fill comment

# Properties
add-highlighter shared/terraform/code/ regex ^\h*(\w+)\b 1:meta

# Function declaration/invocation
add-highlighter shared/terraform/code/ regex \b(\w+)\s*(?:\(|::\s*<) 1:function

# Literals and operators
#add-highlighter shared/terraform/code/ regex [=&|<>?+\-/%!~*\^]|:: 0:operator
add-highlighter shared/terraform/code/ regex \b(?:true|false|null|(?:0x[_0-9a-fA-F]+|0o[_0-7]+|0b[_01]+|[0-9][_0-9]*)|[0-9][_0-9]*(?:\.[0-9][_0-9]*|(?:\.[0-9][_0-9]*)?[eE][\+\-]?[_0-9]+))\b 0:value

# Keywords
add-highlighter shared/terraform/code/ regex \b(?:for|in|if)\b 0:keyword

# Commands
# ~~~~~~~~

define-command -hidden terraform-indent-on-new-line %~
    evaluate-commands -draft -itersel %_
        execute-keys \;
        # Copy comment prefix and following whitespace
        try %< execute-keys -draft k x s '^\h*\K(//|#)\h*' <ret> y gh j P >
        # Preserve previous line indent
        try %< execute-keys -draft K <a-&> >
        # Indent after lines ending with {, (, or [
        try %< execute-keys -draft k x <a-k> '[{(\[]\h*$' <ret> j <a-gt> >
        # Trim trailing whitespace
        try %< execute-keys -draft k x s \h+$ <ret> d >
    _
~

define-command -hidden terraform-indent-on-opening-curly-brace %[
    evaluate-commands -draft -itersel %_
        # align indent with opening paren when { is entered on a new line after the closing paren
        try %[ execute-keys -draft h <a-F> ) M <a-k> \A\(.*\)\h*\n\h*\{\z <ret> s \A|.\z <ret> 1<a-&> ]
    _
]

define-command -hidden terraform-indent-on-closing-curly-brace %[
    evaluate-commands -draft -itersel %_
        # Align to indentation curly brace when alone on a line
        try %[ execute-keys -draft x <a-k> ^\h+\}$ <ret> h <a-a> { <a-S> 1<a-&> ]
    _
]

define-command -hidden terraform-indent-on-closing-paren %[
    evaluate-commands -draft -itersel %_
        # Same but for )
        try %[ execute-keys -draft x <a-k> ^\h+\)$ <ret> h <a-a> ( <a-S> 1<a-&> ]
    _
]

define-command -hidden terraform-indent-on-closing-square %(
    evaluate-commands -draft -itersel %_
        # Same but for ]
        try %{ execute-keys -draft x <a-k> ^\h+\]$ <ret> h <a-a> [ <a-S> 1<a-&> }
    _
)

define-command -hidden terraform-remove-comment-markers %[
    evaluate-commands -draft -itersel %_
        # Remove prefix when joining one comment line to the next
        try %[ execute-keys -draft <a-l> s '\A\h*(//|#)\h*' <ret> c ' ' ]
    _
]

define-command -hidden terraform-remove-trailing-whitespace %[
    evaluate-commands -draft %_
        # Remove trailing whitespace on all lines
        try %[ execute-keys -draft \% s \h+$ <ret> d ]
    _
]

# Initialization
# ~~~~~~~~~~~~~~

hook -group terraform-highlight global WinSetOption filetype=terraform %{
    add-highlighter window/terraform ref terraform
    hook -once -always window WinSetOption filetype=.* %{ remove-highlighter window/terraform }
}

hook global WinSetOption filetype=terraform %[
    hook window InsertChar \n -group terraform-hooks terraform-indent-on-new-line
    hook window InsertChar \{ -group terraform-hooks terraform-indent-on-opening-curly-brace
    hook window InsertChar \} -group terraform-hooks terraform-indent-on-closing-curly-brace
    hook window InsertChar \) -group terraform-hooks terraform-indent-on-closing-paren
    hook window InsertChar \) -group terraform-hooks terraform-indent-on-closing-square
    hook window NormalKey <a-J> -group terraform-hooks terraform-remove-comment-markers
    hook window BufWritePre %val{buffile} -group terraform-hooks terraform-remove-trailing-whitespace
    hook -once -always window WinSetOption filetype=.* %{ remove-hooks window terraform-.+ }
]
