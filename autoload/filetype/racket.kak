# https://racket-lang.org
# ~~~~~~~~~~~~~~~~~~~~~~~

# Detection
# ~~~~~~~~~

hook global BufCreate .*\.rkt %{
    set-option buffer filetype racket
}

# Highlighters

add-highlighter shared/racket regions
add-highlighter shared/racket/code default-region group
add-highlighter shared/racket/string region '"' (?<!\\)(\\\\)*" fill string
add-highlighter shared/racket/comment region ';' '$' fill comment
add-highlighter shared/racket/comment-form region -recurse "\(" "#;\(" "\)" fill comment
add-highlighter shared/racket/comment-block region "#\|" "\|#" fill comment
add-highlighter shared/racket/quoted-form region -recurse "\(" "'\(" "\)" fill variable

add-highlighter shared/racket/code/ regex (#t|#f) 0:value
add-highlighter shared/racket/code/ regex \b[0-9]+(?:\.|/)[0-9]*\b 0:value

# Primitive expressions that cannot be derived.
add-highlighter shared/racket/code/ regex \b(?:define|do|let|let*|letrec|if|cond|case|and|or|begin|lambda|delay|delay-force|set!")\b 0:keyword

# Macro expressions.
add-highlighter shared/racket/code/ regex \b(?:define-syntax|let-syntax|letrec-syntax|syntax-rules|syntax-case)\b 0:keyword

# Racket-specific
add-highlighter shared/racket/code/ regex \b(?:module[*+]?|provide|require)[^\d\w_\-+*/] 0:keyword

# Lang specification
add-highlighter shared/racket/code/ regex "#lang\h+[\w_/-]+" 0:meta

# Function calls
add-highlighter shared/racket/code/ regex "[\(\) ]\(([\w_/\-+*?!][\d\w_/\-+*?!]*)" 1:function

# Basic operators.
add-highlighter shared/racket/code/ regex "[\( ](\*|\+|-|\.\.\.|/|<|<=|=|=>|>|>=)[ ]" 1:operator

# These are all functions, but it is useful to highlight them as types.
add-highlighter shared/racket/code/ regex \b(?:cons|boolean[?]|list[?]?|pair[?]|vector[?]?|bytevector[?]?|string[?]?|char[?]|complex[?]|eof-object[?]|input-port[?]|null[?]|number[?]|output-port[?]|procedure[?]|symbol[?])[^\d\w_/\-+*?!] 0:type

# Commands
# ~~~~~~~~

define-command -hidden racket-indent-on-new-line %~
    evaluate-commands -draft -itersel %_
        execute-keys \;
        # Copy ; comments prefix and following whitespace
        try %< execute-keys -draft k x s '^\h*\K;;?\h*' <ret> y gh j P >
        # Preserve previous line indent
        try %< execute-keys -draft K <a-&> >
        # Trim trailing whitespace
        try %< execute-keys -draft k x s \h+$ <ret> d >
    _
~

define-command -hidden racket-remove-comment-markers %[
    evaluate-commands -draft -itersel %_
        # Remove `;` prefix when joining one comment line to the next
        try %[ execute-keys -draft ';' <a-L> s '\h*;;?\h*' <ret> c ' ' ]
    _
]

define-command -hidden racket-remove-trailing-whitespace %[
    evaluate-commands -draft %_
        # Remove trailing whitespace on all lines
        try %[ execute-keys -draft \% s \h+$ <ret> d ]
    _
]

# Initialization
# ~~~~~~~~~~~~~~

hook global WinSetOption filetype=racket %{
    add-highlighter window/racket ref racket
    hook -once -always window WinSetOption filetype=.* %{ remove-highlighter window/racket }
}

hook global WinSetOption filetype=racket %[
    hook window InsertChar \n -group racket-hooks racket-indent-on-new-line
    hook window NormalKey <a-J> -group racket-hooks racket-remove-comment-markers
    hook window BufWritePre %val{buffile} -group racket-hooks racket-remove-trailing-whitespace
    hook -once -always window WinSetOption filetype=.* %{ remove-hooks window racket-.+ }
]
