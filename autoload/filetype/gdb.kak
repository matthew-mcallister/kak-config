# https://www.gnu.org/software/gdb
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Detection
# ~~~~~~~~~

hook global BufCreate .*\.(?:gdbinit|gdb) %{
    set-option buffer filetype gdb
}

# Highlighters
# ~~~~~~~~~~~~

add-highlighter shared/gdb regions
add-highlighter shared/gdb/prompt default-region group
add-highlighter shared/gdb/commend region '#' $ fill comment
add-highlighter shared/gdb/sh region ^! $ ref sh
add-highlighter shared/gdb/python region ^py(?:thon)?$ ^end$ ref python

# GDB commands
add-highlighter shared/gdb/prompt/ regex \b(?:awatch|break|break-range|catch|clear|commands|condition|delete|disable|dprintf|enable|ftrace|hbreak|ignore|rbreak|rwatch|save|skip|strace|tbreak|tcatch|trace|watch|agent-printf|append|call|disassemble|display|dump|explore|find|init-if-undefined|mem|output|print|print-object|printf|ptype|restore|set|undisplay|whatis|x|add-symbol-file|add-symbol-file-from-memory|cd|core-file|directory|edit|exec-file|file|forward-search|generate-core-file|list|load|nosharedlibrary|path|pwd|remote|remove-symbol-file|reverse-search|search|section|sharedlibrary|symbol-file|flushregs|maintenance|checkpoint|compare-sections|compile|complete|expression|monitor|record|restart|stop|advance|attach|continue|detach|disconnect|finish|handle|inferior|interrupt|jump|kill|next|nexti|queue-signal|reverse-continue|reverse-finish|reverse-next|reverse-nexti|reverse-step|reverse-stepi|run|signal|start|starti|step|stepi|target|task|thread|until|backtrace|down|frame|return|select-frame|up|info|show|add-auto-load-safe-path|add-auto-load-scripts-directory|alias|apropos|define|demangle|document|dont-repeat|down-silently|echo|help|if|interpreter-exec|make|new-ui|overlay|quit|shell|source|up-silently|while|actions|collect|end|passcount|tdump|teval|tfind|tsave|tstart|tstatus|tstop|tvariable|while-stepping|add-inferior|clone-inferior|eval|flash-erase|function|jit-reader-load|jit-reader-unload|remove-inferiors|unset)\b 0:keyword
# Special values and parameters
add-highlighter shared/gdb/prompt/ regex \b(?:on|off)\b 0:value

# Commands
# ~~~~~~~~

define-command -hidden gdb-indent-on-new-line %{
    evaluate-commands -draft -itersel %{
        # copy '#' comment prefix and following white spaces
        try %{ execute-keys -draft k <a-x> s ^\h*#\h* <ret> y jgh P }
        # preserve previous line indent
        try %{ execute-keys -draft \; K <a-&> }
        # indent after line ending with :([
        try %{ execute-keys -draft <space> k x <a-k> [:(\[]$ <ret> j <a-gt> }
    }
}

define-command -hidden gdb-remove-trailing-whitespace %{
    evaluate-commands -draft %{
        # Remove trailing whitespace on all lines
        try %{ execute-keys -draft \% s \h+$ <ret> d }
    }
}

# Initialization
# ~~~~~~~~~~~~~~

hook -group gdb-highlight global WinSetOption filetype=gdb %{
    add-highlighter window/gdb ref gdb
    hook -once -always window WinSetOption filetype=.* %{ remove-highlighter window/gdb }
}

hook global WinSetOption filetype=gdb %[
    hook window InsertChar \n -group gdb-hooks gdb-indent-on-new-line
    hook window BufWritePre %val{buffile} -group gdb-hooks gdb-remove-trailing-whitespace
    hook -once -always window WinSetOption filetype=.* %{ remove-hooks window gdb-.+ }
]
