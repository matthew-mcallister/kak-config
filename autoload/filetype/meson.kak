# https://mesonbuild.com
# ~~~~~~~~~~~~~~~~~~~~~~

# Detection
# ~~~~~~~~~

hook global BufCreate .*/meson\.build %{
    set-option buffer filetype meson
}

# Highlighters
# ~~~~~~~~~~~~

add-highlighter shared/meson regions
add-highlighter shared/meson/code default-region group
add-highlighter shared/meson/triple_string region %{'''} %{'''} fill string
add-highlighter shared/meson/string region %{'} %{(?<!\\)(\\\\)*'} fill string
add-highlighter shared/meson/line_comment region %{#} $ group

add-highlighter shared/meson/line_comment/ regex \b(?:TODO|XXX|FIXME|NOTES?|NB)\b 0:default+r
add-highlighter shared/meson/line_comment/ fill comment

# Literals and operators
add-highlighter shared/meson/code/ regex [=&|<>?+\-/%!~*\^]|:: 0:operator
add-highlighter shared/meson/code/ regex \b(?:true|false)\b 0:value
add-highlighter shared/meson/code/ regex \b(?:0x[0-9a-fA-F]+|0o[0-7]+|0b[01]+|[0-9][0-9]*)\b 0:value

# Keywords
add-highlighter shared/meson/code/ regex \b(?:and|elif|else|endfor|endif|endforeach|endfor|endwhile|foreach|for|if|or|while)\b 0:keyword

# Built-in functions (probably out of date)
add-highlighter shared/meson/code/ regex \b(?:add_global_arguments|add_global_link_arguments|add_languages|add_project_arguments|add_project_link_arguments|add_test_setup|assert|benchmark|both_libraries|build_machine|build_target|configuration_data|configure_file|custom_target|declare_dependency|dependency|disabler|environment|error|executable|files|find_library|find_program|generator|get_option|get_variable|gettext|host_machine|import|include_directories|install_data|install_headers|install_man|install_subdir|is_variable|jar|join_paths|library|meson|message|option|project|run_command|run_target|set_variable|shared_library|shared_module|static_library|subdir|subdir_done|subproject|target_machine|test|vcs_tag|warning)\b 0:function

# Commands
# ~~~~~~~~

define-command -hidden meson-indent-on-new-line %~
    evaluate-commands -draft -itersel %_
        execute-keys \;
        # Copy # comments prefix and following whitespace
        try %< execute-keys -draft k x s '^\h*\K#\h*' <ret> y gh j P >
        # Preserve previous line indent
        try %< execute-keys -draft K <a-&> >
        # Indent after lines ending with {, (, or [
        try %< execute-keys -draft k x <a-k> '[{(\[]\h*$' <ret> j <a-gt> >
    _
~

define-command -hidden meson-indent-on-opening-curly-brace %[
    evaluate-commands -draft -itersel %_
        # align indent with opening paren when { is entered on a new line after the closing paren
        try %[ execute-keys -draft h <a-F> ) M <a-k> \A\(.*\)\h*\n\h*\{\z <ret> s \A|.\z <ret> 1<a-&> ]
    _
]

define-command -hidden meson-indent-on-closing-curly-brace %[
    evaluate-commands -draft -itersel %_
        # Align to indentation curly brace when alone on a line
        try %[ execute-keys -draft x <a-k> ^\h+\}$ <ret> h <a-a> { <a-S> 1<a-&> ]
    _
]

define-command -hidden meson-indent-on-closing-paren %[
    evaluate-commands -draft -itersel %_
        # Same but for )
        try %[ execute-keys -draft x <a-k> ^\h+\)$ <ret> h <a-a> ( <a-S> 1<a-&> ]
    _
]

define-command -hidden meson-indent-on-closing-square %(
    evaluate-commands -draft -itersel %_
        # Same but for ]
        try %{ execute-keys -draft x <a-k> ^\h+\]$ <ret> h <a-a> [ <a-S> 1<a-&> }
    _
)

define-command -hidden meson-remove-comment-markers %[
    evaluate-commands -draft -itersel %_
        # Remove `#` prefix when joining one comment line to the next
        try %[ execute-keys -draft <a-L> s '\h*#\h*' <ret> c ' ' ]
    _
]

define-command -hidden meson-remove-trailing-whitespace %[
    evaluate-commands -draft %_
        # Remove trailing whitespace on all lines
        try %[ execute-keys -draft \% s \h+$ <ret> d ]
    _
]

# Initialization
# ~~~~~~~~~~~~~~

hook -group meson-highlight global WinSetOption filetype=meson %{
    add-highlighter window/meson ref meson
    hook -once -always window WinSetOption filetype=.* %{ remove-highlighter window/meson }
}

hook global WinSetOption filetype=meson %[
    hook window InsertChar \n -group meson-hooks meson-indent-on-new-line
    hook window InsertChar \{ -group meson-hooks meson-indent-on-opening-curly-brace
    hook window InsertChar \} -group meson-hooks meson-indent-on-closing-curly-brace
    hook window InsertChar \) -group meson-hooks meson-indent-on-closing-paren
    hook window InsertChar \) -group meson-hooks meson-indent-on-closing-square
    hook window NormalKey <a-j> -group meson-hooks meson-remove-comment-markers
    hook window BufWritePre %val{buffile} -group meson-hooks meson-remove-trailing-whitespace
    hook -once -always window WinSetOption filetype=.* %{ remove-hooks window meson-.+ }
]
