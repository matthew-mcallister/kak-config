# C++ (WIP)
# ---------

# File detection
# --------------

# C++ file extensions
hook global BufCreate .*\.(cc|cpp|cxx|C|hh|hpp|hxx|H)$ %{
    set-option buffer filetype cpp
}

# Keyword-based filetype detection---far from perfect, as it will
# mistakenly search for keywords inside comments
hook global BufCreate .*\.(h|inl)$ %<
    try %<
        execute-keys -draft %[ %s::|\b(?:template|typename|namespace|constexpr|class)\b|\b(?:public|private|protected)\h*:|<lt>(?:cstdlib|cstdio|string|vector|..?stream)<gt><ret> ]
        set-option buffer filetype cpp
    > catch %<
        set-option buffer filetype c
    >
>

# Hook commands
# -------------

define-command -hidden cpp-indent-on-new-line %<
    evaluate-commands -draft -itersel %<
        execute-keys \;
        # Copy // comment prefix
        try %< execute-keys -draft k x s ^\h*//\h* <ret> y j gh P >
        # Copy * prefix inside block comments
        try %< execute-keys -draft <a-?> /\* <ret> <a-K> \*/ <ret> <a-:> k x s ^\h*\*\h* <ret> y j gh P >
        # Preserve previous line indent
        try %< execute-keys -draft K <a-&> >
        # Indent after lines ending in { or (
        try %< execute-keys -draft k x <a-k> [({]\h*$ <ret> j <a-gt> >
        # Trim trailing whitespace from previous line
        try %< execute-keys -draft k x s \h+$ <ret> d >
    >
>

define-command -hidden cpp-indent-on-opening-curly-brace %<
    evaluate-commands -draft -itersel %<
        # Align indent with opening paren when { is entered on a new line after the closing paren
        try %< execute-keys -draft h <a-F> ) M <a-k> \A\(.*\)\h*\n\h*\{\z <ret> s \A|.\z <ret> 1<a-&> >
    >
>

define-command -hidden cpp-indent-on-closing-curly-brace %<
    evaluate-commands -draft -itersel %<
        # Align to indentation curly brace when alone on a line
        try %< execute-keys -draft x <a-k> ^\h+\}$ <ret> h <a-a> { <a-S> 1<a-&> >
    >
>

define-command -hidden cpp-indent-on-closing-paren %<
    evaluate-commands -draft -itersel %<
        # Same but for )
        try %< execute-keys -draft x <a-k> ^\h+\)$ <ret> h <a-a> ( <a-S> 1<a-&> >
    >
>

define-command -hidden cpp-remove-comment-markers %<
    evaluate-commands -draft -itersel %<
        # Remove `//` prefix when joining one comment line to the next
        try %< execute-keys -draft x <a-k> ^\h*// <ret> s '// ?' <ret> ) <a-space> d >
        # It would be nice to do the `*` prefix when in a block comment,
        # but that doesn't seem possible because the NormalKey hook is
        # called *after* the two lines are joined, so you can't really
        # tell where one line ends and the next begins
    >
>

define-command -hidden cpp-remove-trailing-whitespace %<
    evaluate-commands -draft %<
        # Remove trailing whitespace on all lines
        try %< execute-keys -draft \% s \h+$ <ret> d >
    >
>

# Syntax highlighting
# -------------------

add-highlighter shared/cpp regions
add-highlighter shared/cpp/code default-region group
add-highlighter shared/cpp/string region %{(?<!')(?<!'\\)"} %{(?<!\\)(?:\\\\)*"} fill string
add-highlighter shared/cpp/raw_string region %{R"([^(]*)\(} %{\)([^")]*)"} fill string
add-highlighter shared/cpp/comment region /\* \*/ group
add-highlighter shared/cpp/line_comment region // $ group
add-highlighter shared/cpp/disabled region -recurse "#\h*if(?:def)?" ^\h*?#\h*if\h+(?:0|FALSE)\b "#\h*(?:else|elif|endif)" fill rgb:666666
add-highlighter shared/cpp/macro region %{^\h*?\K#} %{(?<!\\)\n} group
add-highlighter shared/cpp/attribute region \[\s*\[ \]\s*\] fill meta

add-highlighter shared/cpp/macro/ fill meta
add-highlighter shared/cpp/macro/ regex ^\h*#include\h+(\S*) 1:module

add-highlighter shared/cpp/comment/ fill comment
add-highlighter shared/cpp/comment/ regex \b(?:TODO|XXX|FIXME)\b 0:default+r
add-highlighter shared/cpp/line_comment/ fill comment
add-highlighter shared/cpp/line_comment/ regex \b(?:TODO|XXX|FIXME)\b 0:default+r

# Qualified names
add-highlighter shared/cpp/code/ regex \b\w+(?=::) 0:meta

# integer literals
add-highlighter shared/cpp/code/ regex %{(?i)(?<!\.)\b[1-9]('?\d+)*(ul?l?|ll?u?)?\b(?!\.)} 0:value
add-highlighter shared/cpp/code/ regex %{(?i)(?<!\.)\b0b[01]('?[01]+)*(ul?l?|ll?u?)?\b(?!\.)} 0:value
add-highlighter shared/cpp/code/ regex %{(?i)(?<!\.)\b0('?[0-7]+)*(ul?l?|ll?u?)?\b(?!\.)} 0:value
add-highlighter shared/cpp/code/ regex %{(?i)(?<!\.)\b0x[\da-f]('?[\da-f]+)*(ul?l?|ll?u?)?\b(?!\.)} 0:value

# operators
add-highlighter shared/cpp/code/ regex [=&|<>?+\-/%!~*\^]|:: 0:operator

# floating point literals
add-highlighter shared/cpp/code/ regex %{(?i)(?<!\.)\b\d('?\d+)*\.([fl]\b|\B)(?!\.)} 0:value
add-highlighter shared/cpp/code/ regex %{(?i)(?<!\.)\b\d('?\d+)*\.?e[+-]?\d('?\d+)*[fl]?\b(?!\.)} 0:value
add-highlighter shared/cpp/code/ regex %{(?i)(?<!\.)(\b(\d('?\d+)*)|\B)\.\d('?[\d]+)*(e[+-]?\d('?\d+)*)?[fl]?\b(?!\.)} 0:value
add-highlighter shared/cpp/code/ regex %{(?i)(?<!\.)\b0x[\da-f]('?[\da-f]+)*\.([fl]\b|\B)(?!\.)} 0:value
add-highlighter shared/cpp/code/ regex %{(?i)(?<!\.)\b0x[\da-f]('?[\da-f]+)*\.?p[+-]?\d('?\d+)*)?[fl]?\b(?!\.)} 0:value
add-highlighter shared/cpp/code/ regex %{(?i)(?<!\.)\b0x([\da-f]('?[\da-f]+)*)?\.\d('?[\d]+)*(p[+-]?\d('?\d+)*)?[fl]?\b(?!\.)} 0:value

# character literals (no multi-character literals)
add-highlighter shared/cpp/code/ regex %{(\b(u8|u|U|L)|\B)'((\\.)|[^'\\])'\B} 0:value

# Highlight functions
add-highlighter shared/cpp/code/ regex \b\w+\s*(?=\() 0:function

# Keywords
add-highlighter shared/cpp/code/ regex \b(alignas|alignof|and|and_eq|asm|bitand|bitor|break|case|catch|co_await|co_return|co_yield|compl|concept|const_cast|continue|decltype|default|delete|do|dynamic_cast|else|explicit|export|for|goto|if|import|module|new|not|not_eq|operator|or|or_eq|reinterpret_cast|return|sizeof|static_assert|static_cast|switch|throw|try|typeid|using|while|xor|xor_eq|auto|class|enum|extern|final|friend|inline|namespace|noexcept|override|private|protected|public|register|requires|struct|template|typedef|typename|union|virtual)\b 0:keyword
# Attributes
add-highlighter shared/cpp/code/ regex \b(const|consteval|constexpr|mutable|static|thread_local|volatile)\b 0:attribute
# Types
add-highlighter shared/cpp/code/ regex \b(bool|byte|char|char16_t|char32_t|double|float|int|long|max_align_t|nullptr_t|ptrdiff_t|short|signed|size_t|unsigned|void|wchar_t|int8_t|int16_t|int32_t|int64_t|intmax_t|intptr_t|uint8_t|uint16_t|uint32_t|uint64_t|uintmax_t|uintptr_t)\b 0:attribute
# Values
add-highlighter shared/cpp/code/ regex \b(NULL|false|nullptr|this|true)\b 0:value
# Compiler macros
add-highlighter shared/cpp/code/ regex \b(__cplusplus|__STDC_HOSTED__|__FILE__|__LINE__|__DATE__|__TIME__|__STDCPP_DEFAULT_NEW_ALIGNMENT__)\b 0:builtin

# Initialization
# ~~~~~~~~~~~~~~

hook -group cpp-highlight global WinSetOption filetype=cpp %{
    add-highlighter window/cpp ref cpp
    hook -once -always window WinSetOption filetype=.* %{ remove-highlighter window/cpp }
}

hook global WinSetOption filetype=cpp %[
    hook window InsertChar \n -group cpp-hooks cpp-indent-on-new-line
    hook window InsertChar \{ -group cpp-hooks cpp-indent-on-opening-curly-brace
    hook window InsertChar \} -group cpp-hooks cpp-indent-on-closing-curly-brace
    hook window InsertChar \) -group cpp-hooks cpp-indent-on-closing-paren
    hook window NormalKey <a-j> -group cpp-hooks cpp-remove-comment-markers
    hook window NormalKey <a-J> -group cpp-hooks cpp-remove-comment-markers
    # TODO: %val{buffile} needs to be escaped to use as a regex
    hook window BufWritePre %val{buffile} -group cpp-hooks cpp-remove-trailing-whitespace
    hook -once -always window WinSetOption filetype=.* %{ remove-hooks window cpp-.+ }
]
