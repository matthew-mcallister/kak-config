# Thanks to vim-hlsl

# Detection
# ~~~~~~~~~

hook global BufCreate .*\.hlsl %{
    set-option buffer filetype hlsl
}

# TODO: `sampler` is a historically popular variable name in GLSL, but
# it's a highlighted keyword in Vulkan GLSL. Looks ugly.

# Highlighters
# ~~~~~~~~~~~~

add-highlighter shared/hlsl regions
add-highlighter shared/hlsl/code default-region group
add-highlighter shared/hlsl/single_comment region // $ group
add-highlighter shared/hlsl/multi_comment region /\* \*/ group
add-highlighter shared/hlsl/preproc region ^\h*# (?<!\\)\n group
add-highlighter shared/hlsl/attr region ^\h*\[[^\[] \] group
add-highlighter shared/hlsl/attr2 region \[\[ \]\] group

add-highlighter shared/hlsl/single_comment/ fill comment
add-highlighter shared/hlsl/multi_comment/ fill comment
add-highlighter shared/hlsl/preproc/ fill meta
add-highlighter shared/hlsl/attr/ fill meta
add-highlighter shared/hlsl/attr2/ fill meta

# TODO comments
add-highlighter shared/hlsl/single_comment/ regex \b(?:TODO|XXX|FIXME)\b 0:default+r
add-highlighter shared/hlsl/multi_comment/ regex \b(?:TODO|XXX|FIXME)\b 0:default+r

# Literals and operators
add-highlighter shared/hlsl/code/ regex [=+\-/%<>&|~*?\^] 0:operator
add-highlighter shared/hlsl/code/ regex \b(?:(?:[1-9][0-9]*|0[xX][0-9a-fA-F]+|0[0-7]*)[uU]?|(?:[0-9]+[eE][+-]?[0-9]+|(?:[0-9]+\.|[0-9]*\.[0-9]+)(?:[eE][+-]?[0-9]+)?)(?:lf|LF|f|F)?) 0:value

# Swizzles (look nice)
add-highlighter shared/hlsl/code/ regex \.(?:[xyzw]{1,4}|[rgba]{1,4}|[stpq]{1,4})\b 0:operator

# Built-ins

add-highlighter shared/hlsl/code/ regex \b(?:abs|acos|acosh|asin|asinh|atan|atanh|cos|cosh|exp|exp2|floor|log|log10|log2|round|rsqrt|sin|sincos|sinh|sqrt|tan|tanh|trunc)\b 0:function
add-highlighter shared/hlsl/code/ regex \b(?:AllMemoryBarrier|AllMemoryBarrierWithGroupSync|DeviceMemoryBarrier|DeviceMemoryBarrierWithGroupSync|GroupMemoryBarrier|GroupMemoryBarrierWithGroupSync)\b 0:function
add-highlighter shared/hlsl/code/ regex \b(?:abort|clip|errorf|printf)\b 0:function
add-highlighter shared/hlsl/code/ regex \b(?:all|any|countbits|faceforward|firstbithigh|firstbitlow|isfinite|isinf|isnan|max|min|noise|pow|reversebits|sign)\b 0:function
add-highlighter shared/hlsl/code/ regex \b(?:asdouble|asfloat|asint|asuint|D3DCOLORtoUBYTE4|f16tof32|f32tof16)\b 0:function
add-highlighter shared/hlsl/code/ regex \b(?:ceil|clamp|degrees|fma|fmod|frac|frexp|ldexp|lerp|mad|modf|radiants|saturate|smoothstep|step)\b 0:function
add-highlighter shared/hlsl/code/ regex \b(?:cross|determinant|distance|dot|dst|length|lit|msad4|mul|normalize|rcp|reflect|refract|transpose)\b 0:function
add-highlighter shared/hlsl/code/ regex \b(?:ddx|ddx_coarse|ddx_fine|ddy|ddy_coarse|ddy_fine|fwidth)\b 0:function
add-highlighter shared/hlsl/code/ regex \b(?:EvaluateAttributeAtCentroid|EvaluateAttributeAtSample|EvaluateAttributeSnapped)\b 0:function
add-highlighter shared/hlsl/code/ regex \b(?:GetRenderTargetSampleCount|GetRenderTargetSamplePosition)\b 0:function
add-highlighter shared/hlsl/code/ regex \b(?:InterlockedAdd|InterlockedAnd|InterlockedCompareExchange|InterlockedCompareStore|InterlockedExchange|InterlockedMax|InterlockedMin|InterlockedOr|InterlockedXor)\b 0:function
add-highlighter shared/hlsl/code/ regex \b(?:Process2DQuadTessFactorsAvg|Process2DQuadTessFactorsMax|Process2DQuadTessFactorsMin|ProcessIsolineTessFactors)\b 0:function
add-highlighter shared/hlsl/code/ regex \b(?:ProcessQuadTessFactorsAvg|ProcessQuadTessFactorsMax|ProcessQuadTessFactorsMin|ProcessTriTessFactorsAvg|ProcessTriTessFactorsMax|ProcessTriTessFactorsMin)\b 0:function
add-highlighter shared/hlsl/code/ regex \b(?:tex1D|tex1Dbias|tex1Dgrad|tex1Dlod|tex1Dproj)\b 0:function
add-highlighter shared/hlsl/code/ regex \b(?:tex2D|tex2Dbias|tex2Dgrad|tex2Dlod|tex2Dproj)\b 0:function
add-highlighter shared/hlsl/code/ regex \b(?:tex3D|tex3Dbias|tex3Dgrad|tex3Dlod|tex3Dproj)\b 0:function
add-highlighter shared/hlsl/code/ regex \b(?:texCUBE|texCUBEbias|texCUBEgrad|texCUBElod|texCUBEproj)\b 0:function
add-highlighter shared/hlsl/code/ regex \b(?:RestartStrip)\b 0:function
add-highlighter shared/hlsl/code/ regex \b(?:CalculateLevelOfDetail|CalculateLevelOfDetailUnclamped|Gather|GetDimensions|GetSamplePosition|Load|Sample|SampleBias|SampleCmp|SampleCmpLevelZero|SampleGrad|SampleLevel)\b 0:function
add-highlighter shared/hlsl/code/ regex \b(?:Append|Consume|DecrementCounter|IncrementCounter)\b 0:function
add-highlighter shared/hlsl/code/ regex \b(?:Load2|Load3|Load4|Store|Store2|Store3|Store4)\b 0:function
add-highlighter shared/hlsl/code/ regex \b(?:GatherRed|GatherGreen|GatherBlue|GatherAlpha|GatherCmp|GatherCmpRed|GatherCmpGreen|GatherCmpBlue|GatherCmpAlpha)\b 0:function

# Qualifiers
add-highlighter shared/hlsl/code/ regex \b(?:const|row_major|column_major)\b 0:attribute
add-highlighter shared/hlsl/code/ regex \b(?:point|line|triangle|lineadj|triangleadj)\b 0:attribute
add-highlighter shared/hlsl/code/ regex \b(?:InputPatch|OutputPatch)\b 0:attribute
add-highlighter shared/hlsl/code/ regex \b(?:PointStream|LineStream|TriangleStream)\b 0:attribute
add-highlighter shared/hlsl/code/ regex \b(?:in|out|inout)\b 0:attribute
add-highlighter shared/hlsl/code/ regex \b(?:extern|nointerpolation|precise|shared|groupshared|static|uniform|volatile)\b 0:attribute
add-highlighter shared/hlsl/code/ regex \b(?:snorm|unorm)\b 0:attribute
add-highlighter shared/hlsl/code/ regex \b(?:linear|centroid|nointerpolation|noperspective|sample)\b 0:attribute
add-highlighter shared/hlsl/code/ regex \b(?:globallycoherent|register|packoffset)\b 0:attribute

# Special values

add-highlighter shared/hlsl/code/ regex \b(true|false|NULL)\b 0:value

# Semantics (metadata)

add-highlighter shared/hlsl/code/ regex \bSV_(?:Position|InstanceID|PrimitiveID|VertexID|DomainLocation|InsideTessFactor|OutputControlPointID|TessFactor|GSInstanceID|RenderTargetArrayIndex|Coverage|Depth|IsFrontFace|SampleIndex|DispatchThreadID|GroupID|GroupIndex|GroupThreadID)\b 0:meta
add-highlighter shared/hlsl/code/ regex \bSV_(?:ClipDistance\d+|CullDistance\d+|Target[0-7])\b 0:meta

# Types

add-highlighter shared/hlsl/code/ regex \b(?:cbuffer|tbuffer|Buffer|ByteAddressBuffer|ConsumeStructuredBuffer|StructuredBuffer|AppendStructuredBuffer|RWBuffer|RWByteAddressBuffer|RWStructuredBuffer|TextureBuffer|ConstantBuffer)\b 0:type
add-highlighter shared/hlsl/code/ regex \b(?:void|bool|int|uint|dword|half|float|double)\b 0:type
add-highlighter shared/hlsl/code/ regex \b(?:min16float|min10float|min16int|min12int|min16uint)\b 0:type

add-highlighter shared/hlsl/code/ regex \b(?:vector|matrix)\b 0:type
add-highlighter shared/hlsl/code/ regex \b(?:bool|u?int|dword|half|float|double|min16float|min10float|min16int|min12int|min16uint)[1-4](?:x[1-4])?\b 0:type

add-highlighter shared/hlsl/code/ regex \b(?:sampler|sampler1D|sampler2D|sampler3D|samplerCUBE|sampler_state)\b 0:type
add-highlighter shared/hlsl/code/ regex \b(?:SamplerState|SamplerComparisonState)\b 0:type
add-highlighter shared/hlsl/code/ regex \b(?:Texture1D|Texture1DArray|Texture2D|Texture2DArray|Texture2DMS|Texture2DMSArray|Texture3D|TextureCube|TextureCubeArray)\b 0:type
add-highlighter shared/hlsl/code/ regex \b(?:RWTexture1D|RWTexture2D|RWTexture2DArray|RWTexture3D)\b 0:type
add-highlighter shared/hlsl/code/ regex \b(?:texture|texture1D|texture2D|texture3D)\b 0:type

# Keywords

add-highlighter shared/hlsl/code/ regex \b(?:asm|asm_fragment|break|case|class|compile|compile_fragment|continue|default|discard|do|else|export|extern|for|if|interface|namespace|return|struct|switch|typedef|while)\b 0:keyword

# Commands
# ~~~~~~~~

define-command -hidden hlsl-remove-trailing-whitespace %{
    # remove trailing white spaces
    try %{ execute-keys -draft -itersel <a-x> s \h+$ <ret> d }
}

define-command -hidden hlsl-indent-on-new-line %~
    evaluate-commands -draft -itersel %<
        # copy // comments prefix and following white spaces
        try %{ execute-keys -draft k <a-x> s ^\h*\K//[!/]?\h* <ret> y gh j P }
        # preserve previous line indent
        try %{ execute-keys -draft \; K <a-&> }
        # filter previous line
        try %{ execute-keys -draft k : hlsl-filter-around-selections <ret> }
        # indent after lines ending with { or (
        try %[ execute-keys -draft k <a-x> <a-k> [{(]\h*$ <ret> j <a-gt> ]

    >
~

define-command -hidden hlsl-indent-on-opening-curly-brace %[
    evaluate-commands -draft -itersel %_
        # align indent with opening paren when { is entered on a new line after the closing paren
        try %[ execute-keys -draft h <a-F> ) M <a-k> \A\(.*\)\h*\n\h*\{\z <ret> s \A|.\z <ret> 1<a-&> ]
    _
]

define-command -hidden hlsl-indent-on-closing-curly-brace %[
    evaluate-commands -draft -itersel %_
        # align to opening curly brace when alone on a line
        try %[ execute-keys -draft <a-h> <a-k> ^\h+\}$ <ret> h m s \A|.\z <ret> 1<a-&> ]
    _
]

# Initialization
# ~~~~~~~~~~~~~~

hook -group hlsl-highlight global WinSetOption filetype=hlsl %{
    add-highlighter window/hlsl ref hlsl
    hook -once -always window WinSetOption filetype=.* %{ remove-highlighter window/hlsl }
}

hook global WinSetOption filetype=hlsl %[
    hook window InsertChar \n -group hlsl-indent hlsl-indent-on-new-line
    hook window InsertChar \{ -group hlsl-indent hlsl-indent-on-opening-curly-brace
    hook window InsertChar \} -group hlsl-indent hlsl-indent-on-closing-curly-brace
    hook window BufWritePre %val{buffile} -group hlsl-hooks hlsl-remove-trailing-whitespace
    hook -once -always window WinSetOption filetype=.* %{ remove-hooks window hlsl-.+ }
]
