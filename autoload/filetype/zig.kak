# http://ziglang.org
# ~~~~~~~~~~~~~~~~~~

# Detection
# ~~~~~~~~~

hook global BufCreate .*\.zig %{
    set-option buffer filetype zig
}

# Highlighters
# ~~~~~~~~~~~~

add-highlighter shared/zig regions
add-highlighter shared/zig/code default-region group
add-highlighter shared/zig/string region %{(?<!')c?"} (?<!\\)(\\\\)*" fill string
add-highlighter shared/zig/line_string region %{(?<!')c?\\\\} $ fill string
add-highlighter shared/zig/comment region %{//(?!/)} $ group
add-highlighter shared/zig/doc region %{///(?!/)} $ group

add-highlighter shared/zig/comment/ fill comment
add-highlighter shared/zig/comment/ regex \b(?:TODO|XXX|FIXME)\b 0:default+r
add-highlighter shared/zig/doc/ fill meta
add-highlighter shared/zig/doc/ regex \b(?:TODO|XXX|FIXME)\b 0:default+r

# Function declaration/invocation
add-highlighter shared/zig/code/ regex \bfn\s+(\w+) 1:function
add-highlighter shared/zig/code/ regex (:?\b|@)\w+\s*(?=\() 0:function

# Literals and operators
add-highlighter shared/zig/code/ regex [=&|<>?+\-/%!~*\^] 0:operator
add-highlighter shared/zig/code/ regex %{'(?:[^\\]|\\.+?)'} 0:value
add-highlighter shared/zig/code/ regex \b(?:true|false|null|undefined|self)\b 0:value
add-highlighter shared/zig/code/ regex \b(?:0x[_0-9a-fA-F]+|0o[_0-7]+|0b[_01]+|[0-9][_0-9]*)\b 0:value
add-highlighter shared/zig/code/ regex \b[0-9][_0-9]*(?:\.[0-9][_0-9]*|(?:\.[0-9][_0-9]*)?[eE][\+\-]?[_0-9]+)\b 0:value

# Keywords
add-highlighter shared/zig/code/ regex \b(?:and|asm|async|await|break|cancel|catch|continue|defer|else|enum|errdefer|error|export|extern|fn|for|if|inline|nakedcc|or|orelse|packed|promise|pub|resume|return|linksection|stdcallcc|struct|suspend|switch|test|try|union|unreachable|use|var|while)\b 0:keyword
add-highlighter shared/zig/code/ regex \b(?:align|comptime|const|noalias|volatile)\b 0:attribute
add-highlighter shared/zig/code/ regex \b[iu][0-9]+\b 0:type
add-highlighter shared/zig/code/ regex \b(?:usize|isize|f16|f32|f64|f128|bool|void|noreturn|type|anyerror|comptime_int|comptime_float|c_short|c_ushort|c_int|c_uint|c_long|c_ulong|c_longlong|c_ulonglong|c_longdouble|c_void)\b 0:type

# Commands
# ~~~~~~~~

define-command -hidden zig-indent-on-new-line %~
    evaluate-commands -draft -itersel %_
        execute-keys \;
        # Copy // comments prefix and following white spaces
        try %< execute-keys -draft k x s ^\h*\K//[!/]?\h* <ret> y gh j P >
        # Preserve previous line indent
        try %< execute-keys -draft K <a-&> >
        # Indent after lines ending with {, (, or [
        try %< execute-keys -draft k x <a-k> [{(\[]\h*$ <ret> j <a-gt> >
        # Trim trailing whitespace
        try %< execute-keys -draft k x s \h+$ <ret> d >
    _
~

define-command -hidden zig-indent-on-opening-curly-brace %[
    evaluate-commands -draft -itersel %_
        # align indent with opening paren when { is entered on a new line after the closing paren
        try %[ execute-keys -draft h <a-F> ) M <a-k> \A\(.*\)\h*\n\h*\{\z <ret> s \A|.\z <ret> 1<a-&> ]
    _
]

define-command -hidden zig-indent-on-closing-curly-brace %[
    evaluate-commands -draft -itersel %_
        # Align to indentation curly brace when alone on a line
        try %[ execute-keys -draft x <a-k> ^\h+\}$ <ret> h <a-a> { <a-S> 1<a-&> ]
    _
]

define-command -hidden zig-indent-on-closing-paren %[
    evaluate-commands -draft -itersel %_
        # Same but for )
        try %[ execute-keys -draft x <a-k> ^\h+\)$ <ret> h <a-a> ( <a-S> 1<a-&> ]
    _
]

define-command -hidden zig-indent-on-closing-square %(
    evaluate-commands -draft -itersel %_
        # Same but for ]
        try %{ execute-keys -draft x <a-k> ^\h+\]$ <ret> h <a-a> [ <a-S> 1<a-&> }
    _
)

define-command -hidden zig-remove-comment-markers %[
    evaluate-commands -draft -itersel %_
        # Remove `//` prefix when joining one comment line to the next
        try %[ execute-keys -draft x <a-k> ^\h*// <ret> s '//[!/]? ?' <ret> ) <a-space> d ]
    _
]

define-command -hidden zig-remove-trailing-whitespace %[
    evaluate-commands -draft %_
        # Remove trailing whitespace on all lines
        try %[ execute-keys -draft \% s \h+$ <ret> d ]
    _
]

# Initialization
# ~~~~~~~~~~~~~~


hook -group zig-highlight global WinSetOption filetype=zig %{
    add-highlighter window/zig ref zig
    hook -once -always window WinSetOption filetype=.* %{ remove-highlighter window/zig }
}

hook global WinSetOption filetype=zig %[
    hook window InsertChar \n -group zig-hooks zig-indent-on-new-line
    hook window InsertChar \{ -group zig-hooks zig-indent-on-opening-curly-brace
    hook window InsertChar \} -group zig-hooks zig-indent-on-closing-curly-brace
    hook window InsertChar \) -group zig-hooks zig-indent-on-closing-paren
    hook window InsertChar \) -group zig-hooks zig-indent-on-closing-square
    hook window NormalKey <a-j> -group zig-hooks zig-remove-comment-markers
    hook window NormalKey <a-J> -group zig-hooks zig-remove-comment-markers
    hook window BufWritePre %val{buffile} -group zig-hooks zig-remove-trailing-whitespace
    hook -once -always window WinSetOption filetype=.* %{ remove-hooks window zig-.+ }
]
