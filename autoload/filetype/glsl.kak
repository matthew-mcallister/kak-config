# https://github.com/KhronosGroup/glslang
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Thanks to vim-glsl

# Detection
# ~~~~~~~~~

hook global BufCreate .*\.(?:glsl|vert|tesc|tese|geom|frag|comp|vs|fs|gs) %{
    set-option buffer filetype glsl
}

# TODO: `sampler` is a historically popular variable name in GLSL, but
# it's a highlighted keyword in Vulkan GLSL. Looks ugly.

# Highlighters
# ~~~~~~~~~~~~

add-highlighter shared/glsl regions
add-highlighter shared/glsl/code default-region group
add-highlighter shared/glsl/single_comment region // $ group
add-highlighter shared/glsl/multi_comment region /\* \*/ group
add-highlighter shared/glsl/preproc region ^\h*# (?<!\\)\n group

add-highlighter shared/glsl/single_comment/ fill comment
add-highlighter shared/glsl/multi_comment/ fill comment
add-highlighter shared/glsl/preproc/ fill meta

# TODO comments
add-highlighter shared/glsl/single_comment/ regex \b(?:TODO|XXX|FIXME)\b 0:default+r
add-highlighter shared/glsl/multi_comment/ regex \b(?:TODO|XXX|FIXME)\b 0:default+r

# Literals and operators
add-highlighter shared/glsl/code/ regex [!=+\-/%<>&|~*?:\^] 0:operator
add-highlighter shared/glsl/code/ regex \b(?:true|false|__LINE__|__FILE__|__VERSION__|GL_ES|VULKAN)\b 0:value
add-highlighter shared/glsl/code/ regex \b(?:(?:[1-9][0-9]*|0[xX][0-9a-fA-F]+|0[0-7]*)[uU]?|(?:[0-9]+[eE][+-]?[0-9]+|(?:[0-9]+\.|[0-9]*\.[0-9]+)(?:[eE][+-]?[0-9]+)?)(?:lf|LF|f|F)?) 0:value

# Swizzles (look nice)
add-highlighter shared/glsl/code/ regex \.(?:[xyzw]{1,4}|[rgba]{1,4}|[stpq]{1,4})\b 0:operator

# Keywords
add-highlighter shared/glsl/code/ regex \b(?:if|else|for|while|do|discard|return|break|continue|struct)\b 0:keyword
add-highlighter shared/glsl/code/ regex \b(?:attribute|layout|in|inout|out|uniform|varying)\b 0:keyword
add-highlighter shared/glsl/code/ regex %{\b(?:align|binding|buffer|ccw|centroid|centroid varying|coherent|column_major|const|constant_id|cw|depth_any|depth_greater|depth_less|depth_unchanged|early_fragment_tests|equal_spacing|flat|fractional_even_spacing|fractional_odd_spacing|highp|index|invariant|invocations|isolines|line_strip|lines|lines_adjacency|local_size_x|local_size_x_id|local_size_y|local_size_y_id|local_size_z|local_size_z_id|location|lowp|max_vertices|mediump|noperspective|offset|origin_upper_left|packed|patch|pixel_center_integer|point_mode|points|precise|precision|push_constant|quads|r11f_g11f_b10f|r16|r16_snorm|r16f|r16i|r16ui|r32f|r32i|r32ui|r8|r8_snorm|r8i|r8ui|readonly|restrict|rg16|rg16_snorm|rg16f|rg16i|rg16ui|rg32f|rg32i|rg32ui|rg8|rg8_snorm|rg8i|rg8ui|rgb10_a2|rgb10_a2ui|rgba16|rgba16_snorm|rgba16f|rgba16i|rgba16ui|rgba32f|rgba32i|rgba32ui|rgba8|rgba8_snorm|rgba8i|rgba8ui|row_major|sample|set|shared|smooth|std140|std430|stream|triangle_strip|triangles|triangles_adjacency|vertices|volatile|writeonly|xfb_buffer|xfb_stride|xfb_offset)\b} 0:attribute
add-highlighter shared/glsl/code/ regex \b(?:atomic_uint|bool|bvec2|bvec3|bvec4|dmat2|dmat2x2|dmat2x3|dmat2x4|dmat3|dmat3x2|dmat3x3|dmat3x4|dmat4|dmat4x2|dmat4x3|dmat4x4|double|dvec2|dvec3|dvec4|float|iimage1D|iimage1DArray|iimage2D|iimage2DArray|iimage2DMS|iimage2DMSArray|iimage2DRect|iimage3D|iimageBuffer|iimageCube|iimageCubeArray|image1D|image1DArray|image2D|image2DArray|image2DMS|image2DMSArray|image2DRect|image3D|imageBuffer|imageCube|imageCubeArray|int|isampler1D|isampler1DArray|isampler2D|isampler2DArray|isampler2DMS|isampler2DMSArray|isampler2DRect|isampler3D|isamplerBuffer|isamplerCube|isamplerCubeArray|isubpassInput|isubpassInputMS|itexture1D|itexture1DArray|itexture2D|itexture2DArray|itexture2DMS|itexture2DMSArray|itexture2DRect|itexture3D|itextureBuffer|itextureCube|itextureCubeArray|ivec2|ivec3|ivec4|mat2|mat2x2|mat2x3|mat2x4|mat3|mat3x2|mat3x3|mat3x4|mat4|mat4x2|mat4x3|mat4x4|sampler|sampler1D|sampler1DArray|sampler1DArrayShadow|sampler1DShadow|sampler2D|sampler2DArray|sampler2DArrayShadow|sampler2DMS|sampler2DMSArray|sampler2DRect|sampler2DRectShadow|sampler2DShadow|sampler3D|samplerBuffer|samplerCube|samplerCubeArray|samplerCubeArrayShadow|samplerCubeShadow|samplerShadow|subpassInput|subpassInputMS|texture1D|texture1DArray|texture2D|texture2DArray|texture2DMS|texture2DMSArray|texture2DRect|texture3D|textureBuffer|textureCube|textureCubeArray|uimage1D|uimage1DArray|uimage2D|uimage2DArray|uimage2DMS|uimage2DMSArray|uimage2DRect|uimage3D|uimageBuffer|uimageCube|uimageCubeArray|uint|usampler1D|usampler1DArray|usampler2D|usampler2DArray|usampler2DMS|usampler2DMSArray|usampler2DRect|usampler3D|usamplerBuffer|usamplerCube|usamplerCubeArray|usubpassInput|usubpassInputMS|utexture1D|utexture1DArray|utexture2D|utexture2DArray|utexture2DMS|utexture2DMSArray|utexture2DRect|utexture3D|utextureBuffer|utextureCube|utextureCubeArray|uvec2|uvec3|uvec4|vec2|vec3|vec4|void)\b 0:type

# Built-ins
add-highlighter shared/glsl/code/ regex \bgl_(?:CullDistance|MaxAtomicCounterBindings|MaxAtomicCounterBufferSize|MaxClipDistances|MaxClipPlanes|MaxCombinedAtomicCounterBuffers|MaxCombinedAtomicCounters|MaxCombinedClipAndCullDistances|MaxCombinedImageUniforms|MaxCombinedImageUnitsAndFragmentOutputs|MaxCombinedShaderOutputResources|MaxCombinedTextureImageUnits|MaxComputeAtomicCounterBuffers|MaxComputeAtomicCounters|MaxComputeImageUniforms|MaxComputeTextureImageUnits|MaxComputeUniformComponents|MaxComputeWorkGroupCount|MaxComputeWorkGroupSize|MaxCullDistances|MaxDrawBuffers|MaxFragmentAtomicCounterBuffers|MaxFragmentAtomicCounters|MaxFragmentImageUniforms|MaxFragmentInputComponents|MaxFragmentInputVectors|MaxFragmentUniformComponents|MaxFragmentUniformVectors|MaxGeometryAtomicCounterBuffers|MaxGeometryAtomicCounters|MaxGeometryImageUniforms|MaxGeometryInputComponents|MaxGeometryOutputComponents|MaxGeometryOutputVertices|MaxGeometryTextureImageUnits|MaxGeometryTotalOutputComponents|MaxGeometryUniformComponents|MaxGeometryVaryingComponents|MaxImageSamples|MaxImageUnits|MaxLights|MaxPatchVertices|MaxProgramTexelOffset|MaxSamples|MaxTessControlAtomicCounterBuffers|MaxTessControlAtomicCounters|MaxTessControlImageUniforms|MaxTessControlInputComponents|MaxTessControlOutputComponents|MaxTessControlTextureImageUnits|MaxTessControlTotalOutputComponents|MaxTessControlUniformComponents|MaxTessEvaluationAtomicCounterBuffers|MaxTessEvaluationAtomicCounters|MaxTessEvaluationImageUniforms|MaxTessEvaluationInputComponents|MaxTessEvaluationOutputComponents|MaxTessEvaluationTextureImageUnits|MaxTessEvaluationUniformComponents|MaxTessGenLevel|MaxTessPatchComponents|MaxTextureCoords|MaxTextureImageUnits|MaxTextureUnits|MaxTransformFeedbackBuffers|MaxTransformFeedbackInterleavedComponents|MaxVaryingComponents|MaxVaryingFloats|MaxVaryingVectors|MaxVertexAtomicCounterBuffers|MaxVertexAtomicCounters|MaxVertexAttribs|MaxVertexImageUniforms|MaxVertexOutputComponents|MaxVertexOutputVectors|MaxVertexTextureImageUnits|MaxVertexUniformComponents|MaxVertexUniformVectors|MaxViewports|MinProgramTexelOffset)\b 0:value
add-highlighter shared/glsl/code/ regex \bgl_(?:BackColor|BackLightModelProduct|BackLightProduct|BackLightProduct|BackMaterial|BackSecondaryColor|ClipDistance|ClipPlane|ClipVertex|Color|DepthRange|EyePlaneQ|EyePlaneR|EyePlaneS|EyePlaneT|Fog|FogCoord|FogFragCoord|FragColor|FragCoord|FragData|FragDepth|FrontColor|FrontFacing|FrontLightModelProduct|FrontLightProduct|FrontMaterial|FrontSecondaryColor|GlobalInvocationID|HelperInvocation|InstanceID|InstanceIndex|InvocationID|Layer|LightModel|LightSource|LocalInvocationID|LocalInvocationIndex|ModelViewMatrix|ModelViewMatrixInverse|ModelViewMatrixInverseTranspose|ModelViewMatrixTranspose|ModelViewProjectionMatrix|ModelViewProjectionMatrixInverse|ModelViewProjectionMatrixInverseTranspose|ModelViewProjectionMatrixTranspose|MultiTexCoord0|MultiTexCoord1|MultiTexCoord2|MultiTexCoord3|MultiTexCoord4|MultiTexCoord5|MultiTexCoord6|MultiTexCoord7|Normal|NormalMatrix|NormalScale|NumSamples|NumWorkGroups|ObjectPlaneQ|ObjectPlaneR|ObjectPlaneS|ObjectPlaneT|PatchVerticesIn|Point|PointCoord|PointSize|Position|PrimitiveID|PrimitiveIDIn|ProjectionMatrix|ProjectionMatrixInverse|ProjectionMatrixInverseTranspose|ProjectionMatrixTranspose|SampleID|SampleMask|SampleMaskIn|SamplePosition|SecondaryColor|TessCoord|TessLevelInner|TessLevelOuter|TexCoord|TextureEnvColor|TextureMatrix|TextureMatrixInverse|TextureMatrixInverseTranspose|TextureMatrixTranspose|Vertex|VertexID|VertexIndex|ViewportIndex|WorkGroupID|WorkGroupSize|in|out)\b 0:value
# Only built-in f'ns are highlighted to help prevent typos
add-highlighter shared/glsl/code/ regex \b(?:EmitStreamVertex|EmitVertex|EndPrimitive|EndStreamPrimitive|abs|acos|acosh|all|any|asin|asinh|atan|atanh|atomicAdd|atomicAnd|atomicCompSwap|atomicCounter|atomicCounterDecrement|atomicCounterIncrement|atomicExchange|atomicMax|atomicMin|atomicOr|atomicXor|barrier|bitCount|bitfieldExtract|bitfieldInsert|bitfieldReverse|ceil|clamp|cos|cosh|cross|dFdx|dFdxCoarse|dFdxFine|dFdy|dFdyCoarse|dFdyFine|degrees|determinant|distance|dot|equal|exp|exp2|faceforward|findLSB|findMSB|floatBitsToInt|floatBitsToUint|floor|fma|fract|frexp|ftransform|fwidth|fwidthCoarse|fwidthFine|greaterThan|greaterThanEqual|groupMemoryBarrier|imageAtomicAdd|imageAtomicAnd|imageAtomicCompSwap|imageAtomicExchange|imageAtomicMax|imageAtomicMin|imageAtomicOr|imageAtomicXor|imageLoad|imageSize|imageStore|imulExtended|intBitsToFloat|interpolateAtCentroid|interpolateAtOffset|interpolateAtSample|inverse|inversesqrt|isinf|isnan|ldexp|length|lessThan|lessThanEqual|log|log2|matrixCompMult|max|memoryBarrier|memoryBarrierAtomicCounter|memoryBarrierBuffer|memoryBarrierImage|memoryBarrierShared|min|mix|mod|modf|noise1|noise2|noise3|noise4|normalize|not|notEqual|outerProduct|packDouble2x32|packHalf2x16|packSnorm2x16|packSnorm4x8|packUnorm2x16|packUnorm4x8|pow|radians|reflect|refract|round|roundEven|shadow1D|shadow1DLod|shadow1DProj|shadow1DProjLod|shadow2D|shadow2DLod|shadow2DProj|shadow2DProjLod|sign|sin|sinh|smoothstep|sqrt|step|tan|tanh|texelFetch|texelFetchOffset|texture|texture1DLod|texture1DProj|texture1DProjLod|texture2DLod|texture2DProj|texture2DProjLod|texture3DLod|texture3DProj|texture3DProjLod|textureCube|textureCubeLod|textureGather|textureGatherOffset|textureGatherOffsets|textureGrad|textureGradOffset|textureLod|textureLodOffset|textureOffset|textureProj|textureProjGrad|textureProjGradOffset|textureProjLod|textureProjLodOffset|textureProjOffset|textureQueryLevels|textureQueryLod|textureSize|transpose|trunc|uaddCarry|uintBitsToFloat|umulExtended|unpackDouble2x32|unpackHalf2x16|unpackSnorm2x16|unpackSnorm4x8|unpackUnorm2x16|unpackUnorm4x8|usubBorrow)\b 0:function
# Hack: some types are also functions, so highlight all types used like f'ns
add-highlighter shared/glsl/code/ regex \b(?:atomic_uint|bool|bvec2|bvec3|bvec4|dmat2|dmat2x2|dmat2x3|dmat2x4|dmat3|dmat3x2|dmat3x3|dmat3x4|dmat4|dmat4x2|dmat4x3|dmat4x4|double|dvec2|dvec3|dvec4|float|iimage1D|iimage1DArray|iimage2D|iimage2DArray|iimage2DMS|iimage2DMSArray|iimage2DRect|iimage3D|iimageBuffer|iimageCube|iimageCubeArray|image1D|image1DArray|image2D|image2DArray|image2DMS|image2DMSArray|image2DRect|image3D|imageBuffer|imageCube|imageCubeArray|int|isampler1D|isampler1DArray|isampler2D|isampler2DArray|isampler2DMS|isampler2DMSArray|isampler2DRect|isampler3D|isamplerBuffer|isamplerCube|isamplerCubeArray|isubpassInput|isubpassInputMS|itexture1D|itexture1DArray|itexture2D|itexture2DArray|itexture2DMS|itexture2DMSArray|itexture2DRect|itexture3D|itextureBuffer|itextureCube|itextureCubeArray|ivec2|ivec3|ivec4|mat2|mat2x2|mat2x3|mat2x4|mat3|mat3x2|mat3x3|mat3x4|mat4|mat4x2|mat4x3|mat4x4|sampler|sampler1D|sampler1DArray|sampler1DArrayShadow|sampler1DShadow|sampler2D|sampler2DArray|sampler2DArrayShadow|sampler2DMS|sampler2DMSArray|sampler2DRect|sampler2DRectShadow|sampler2DShadow|sampler3D|samplerBuffer|samplerCube|samplerCubeArray|samplerCubeArrayShadow|samplerCubeShadow|samplerShadow|subpassInput|subpassInputMS|texture1D|texture1DArray|texture2D|texture2DArray|texture2DMS|texture2DMSArray|texture2DRect|texture3D|textureBuffer|textureCube|textureCubeArray|uimage1D|uimage1DArray|uimage2D|uimage2DArray|uimage2DMS|uimage2DMSArray|uimage2DRect|uimage3D|uimageBuffer|uimageCube|uimageCubeArray|uint|usampler1D|usampler1DArray|usampler2D|usampler2DArray|usampler2DMS|usampler2DMSArray|usampler2DRect|usampler3D|usamplerBuffer|usamplerCube|usamplerCubeArray|usubpassInput|usubpassInputMS|utexture1D|utexture1DArray|utexture2D|utexture2DArray|utexture2DMS|utexture2DMSArray|utexture2DRect|utexture3D|utextureBuffer|utextureCube|utextureCubeArray|uvec2|uvec3|uvec4|vec2|vec3|vec4|void)(?=\() 0:function

# Commands
# ~~~~~~~~

define-command -hidden glsl-remove-trailing-whitespace %{
    # Remove trailing whitespace on all lines
    try %[ execute-keys -draft \% s \h+$ <ret> d ]
}

define-command -hidden glsl-indent-on-new-line %~
    evaluate-commands -draft -itersel %<
        # copy // comments prefix and following white spaces
        try %{ execute-keys -draft k <a-x> s ^\h*\K//[!/]?\h* <ret> y gh j P }
        # preserve previous line indent
        try %{ execute-keys -draft \; K <a-&> }
        # filter previous line
        try %{ execute-keys -draft k : glsl-filter-around-selections <ret> }
        # indent after lines ending with { or (
        try %[ execute-keys -draft k <a-x> <a-k> [{(]\h*$ <ret> j <a-gt> ]
    >
~

define-command -hidden glsl-indent-on-opening-curly-brace %[
    evaluate-commands -draft -itersel %_
        # align indent with opening paren when { is entered on a new line after the closing paren
        try %[ execute-keys -draft h <a-F> ) M <a-k> \A\(.*\)\h*\n\h*\{\z <ret> s \A|.\z <ret> 1<a-&> ]
    _
]

define-command -hidden glsl-indent-on-closing-curly-brace %[
    evaluate-commands -draft -itersel %_
        # align to opening curly brace when alone on a line
        try %[ execute-keys -draft <a-h> <a-k> ^\h+\}$ <ret> h m s \A|.\z <ret> 1<a-&> ]
    _
]

# Initialization
# ~~~~~~~~~~~~~~

hook -group glsl-highlight global WinSetOption filetype=glsl %{
    add-highlighter window/glsl ref glsl
    hook -once -always window WinSetOption filetype=.* %{ remove-highlighter window/glsl }
}

hook global WinSetOption filetype=glsl %[
    hook window InsertChar \n -group glsl-indent glsl-indent-on-new-line
    hook window InsertChar \{ -group glsl-indent glsl-indent-on-opening-curly-brace
    hook window InsertChar \} -group glsl-indent glsl-indent-on-closing-curly-brace
    hook window BufWritePre %val{buffile} -group glsl-hooks glsl-remove-trailing-whitespace
    hook -once -always window WinSetOption filetype=.* %{ remove-hooks window glsl-.+ }
]
