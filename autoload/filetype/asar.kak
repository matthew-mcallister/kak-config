# asar (SNES assmebler)
# ---------------------

# File detection
# --------------

# .asm file extension
# TODO: Support multiple assembly types
hook global BufCreate .*\.asm$ %{
    set-option buffer filetype asar
}

# Hook commands
# -------------

define-command -hidden asar-indent-on-new-line %<
    evaluate-commands -draft -itersel %<
        execute-keys \;
        # Copy ; comment prefix
        try %< execute-keys -draft k x s ^\h*;\h* <ret> y j gh P >
        # Preserve previous line indent
        try %< execute-keys -draft K <a-&> >
        # Indent after keywords
        try %< execute-keys -draft k x <a-k> ^\h*(?:macro|if|else|elseif) <ret> j <a-gt> >
    >
>

define-command -hidden asar-remove-comment-markers %<
    evaluate-commands -draft -itersel %<
        # Remove `;` prefix when joining one comment line to the next
        try %< execute-keys -draft <a-l> s \A\h*;\h* <ret> c ' ' >
    >
>

define-command -hidden asar-remove-trailing-whitespace %[
    evaluate-commands -draft %_
        # Remove trailing whitespace on all lines
        try %[ execute-keys -draft \% s \h+$ <ret> d ]
    _
]

# Syntax highlighting
# -------------------

add-highlighter shared/asar regions
add-highlighter shared/asar/code default-region group
add-highlighter shared/asar/string region %{(?<!')(?<!'\\)"} %{(?<!\\)(?:\\\\)*"} fill string
add-highlighter shared/asar/line_comment region ';' '$' group

add-highlighter shared/asar/line_comment/ fill comment
add-highlighter shared/asar/line_comment/ regex \b(?:TODO|XXX|FIXME)\b 0:default+r

# operators
add-highlighter shared/asar/code/ regex [=&|?+\-/%!~*\^#<>] 0:operator

# integer literals
add-highlighter shared/asar/code/ regex %{(?i)(?<!\.)\b\d+(?!\.)} 0:value
add-highlighter shared/asar/code/ regex %{%[01]+} 0:value
add-highlighter shared/asar/code/ regex %{(?i)\$[\da-f]+} 0:value

# Highlight function/macro calls
add-highlighter shared/asar/code/ regex (?<![%])[a-zA-Z_]\w*\s*(?=\() 0:function
add-highlighter shared/asar/code/ regex \%[a-zA-Z_]\w*\s*(?=\() 0:meta

# Keywords
add-highlighter shared/asar/code/ regex (?i)(?<!\$)\b(?:print|align|struct|endstruct|namespace|arch|base|padbyte|pad|lorom|math|incbin|incsrc|pushpc|pullpc|skip|function|db|dw|dl|org|macro|endmacro|if|else|elseif|endif)\b 0:keyword
# Special args
add-highlighter shared/asar/code/ regex \b(?:nested|pri|on|off)\b 0:attribute

# Instructions (65816)
add-highlighter shared/asar/code/ regex (?i)\b(?:adc|and|asl|bcc|bcs|beq|bit|bmi|bne|bpl|bra|brk|brl|bvc|bvs|clc|cld|cli|clv|cmp|cpx|cpy|cop|dec|dex|dey|eor|inc|inx|iny|jmp|jml|jsr|jsl|lda|ldx|ldy|lsr|mvn|mvp|nop|ora|pea|pei|per|pha|phb|phd|phk|php|phx|phy|pla|plb|pld|plp|plx|ply|rep|rol|ror|rti|rts|rtl|sbc|sec|sed|sei|sep|sta|stp|stx|sty|stz|tax|tay|tcd|tcs|tdc|trb|tsb|tsc|tsx|txa|txs|txy|tya|tyx|wai|wdm|xba|xce)(?:\.[bwl])?\b 0:keyword
# Instructions (SPC700)
add-highlighter shared/asar/code/ regex (?i)\b(?:adc|adcz|and|andz|and1|asl|aslzlsr|lsrz|rol|rolz|ror|rorz|bbc[0-7]|bbs[0-7]|bpl|bra|bmi|bvc|bvs|bcc|bcs|bne|bew|clr[0-7]|set[0-7]|cmp|cmpz|cbne|dbnz|daa|das|not1|xcn|mov1|decw|incw|clrw|addw|subw|movw|mul|div|eor|eorz|eor1|dec|decz|inc|incz|mov|movz|or|orz|or1|sbc|sbcz|tcall|tset1|tclr1|call|pcall|jmp|push|pop|nop|brk|ret|reti|clrp|setp|clrc|setc|ei|di|clrv|notc|sleep|stop)\b 0:keyword
# Instruction syntax
add-highlighter shared/asar/code/ regex \b(?:[AXY]|SP|YA)\b 0:keyword
add-highlighter shared/asar/code/ regex ,[XY] 0:keyword

# Macro variable instance
add-highlighter shared/asar/code/ regex <\w+> 0:attribute

# Labels
add-highlighter shared/asar/code/ regex ^\h*#?\??(\.*[A-Za-z0-9_]+): 1:function

# Defines
add-highlighter shared/asar/code/ regex ![A-Za-z0-9_]+\b 0:meta

# Initialization
# ~~~~~~~~~~~~~~

hook -group asar-highlight global WinSetOption filetype=asar %{
    add-highlighter window/asar ref asar
    hook -once -always window WinSetOption filetype=.* %{ remove-highlighter window/a
sm}
}

hook global WinSetOption filetype=asar %[
    hook window InsertChar \n -group asar-hooks asar-indent-on-new-line
    hook window NormalKey <a-j> -group asar-hooks asar-remove-comment-markers
    hook window NormalKey <a-J> -group asar-hooks asar-remove-comment-markers
    # TODO: %val{buffile} needs to be escaped to use as a regex
    hook window BufWritePre %val{buffile} -group asar-hooks asar-remove-trailing-whitespace
    hook -once -always window WinSetOption filetype=.* %{ remove-hooks window asar-.+ }
]
