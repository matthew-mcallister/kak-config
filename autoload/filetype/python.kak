# http://python.org
# -----------------

# Detection
# ---------

hook global BufCreate .*[.](py) %{
    set-option buffer filetype python
}

# Highlighters & Completion
# -------------------------

add-highlighter shared/python regions
add-highlighter shared/python/code default-region group
add-highlighter shared/python/docstring region -match-capture (?:"""|''') (?:"""|''') regions
add-highlighter shared/python/double_string region (?i)(?<!f)r?" (?<!\\)(?:\\\\)*" fill string
add-highlighter shared/python/single_string region (?i)(?<!f)r?' (?<!\\)(?:\\\\)*' fill string
add-highlighter shared/python/double_fstring region (?i)(?:r?f|fr?)" (?<!\\)(?:\\\\)*" regions
add-highlighter shared/python/single_fstring region (?i)(?:rf?|fr?)' (?<!\\)(?:\\\\)*' regions
add-highlighter shared/python/comment region '#' '$' fill comment

# TODO: These rules incorrectly highlight {{ and }}

add-highlighter shared/python/double_fstring/ default-region fill string
add-highlighter shared/python/double_fstring/ region (?<!\{)(?:\{\{)*\{ \} ref python

add-highlighter shared/python/single_fstring/ default-region fill string
add-highlighter shared/python/single_fstring/ region (?<!\{)(?:\{\{)*\{ \} ref python

add-highlighter shared/python/docstring/ default-region fill string
add-highlighter shared/python/docstring/ region '>>> \K' '\z' ref python
add-highlighter shared/python/docstring/ region '\.\.\. \K' '\z' ref python

# Integer formats
add-highlighter shared/python/code/ regex '(?i)\b0b[01_]*[01]l?\b' 0:value
add-highlighter shared/python/code/ regex '(?i)\b0x[\da-f_]*[\da-f]l?\b' 0:value
add-highlighter shared/python/code/ regex '(?i)\b0o?[0-7_]*[0-7]l?\b' 0:value
add-highlighter shared/python/code/ regex '(?i)\b([1-9]([\d_]*\d)?|0)l?\b' 0:value
# Float formats
add-highlighter shared/python/code/ regex '\b\d+[eE][+-]?\d+\b' 0:value
add-highlighter shared/python/code/ regex '(\b\d+)?\.\d+\b' 0:value
add-highlighter shared/python/code/ regex '\b\d+\.' 0:value
# Imaginary formats
add-highlighter shared/python/code/ regex '\b\d+\+\d+[jJ]\b' 0:value

# Predefined values
add-highlighter shared/python/code/ regex \b(?:True|False|None|self|inf)\b 0:value
add-highlighter shared/python/code/ regex \b(?:__annotations__|__closure__|__code__|__defaults__|__dict__|__doc__|__globals__|__kwdefaults__|__module__|__name__|__qualname__)\b 0:value

# Functions
add-highlighter shared/python/code/ regex \w+\s*(?=\() 0:function

# Decorators
add-highlighter shared/python/code/ regex @\w+ 0:meta

# Types
add-highlighter shared/python/code/ regex \b(?:bool|buffer|bytearray|bytes|complex|dict|file|float|frozenset|int|list|long|memoryview|object|set|str|tuple|type|unicode|xrange)\b 0:type

# built-in exceptions (see https://docs.python.org/3/library/exceptions.html)
add-highlighter shared/python/code/ regex \b(?:ArithmeticError|AssertionError|AttributeError|BaseException|BlockingIOError|BrokenPipeError|BufferError|BytesWarning|ChildProcessError|ConnectionAbortedError|ConnectionError|ConnectionRefusedError|ConnectionResetError|DeprecationWarning|EOFError|Exception|FileExistsError|FileNotFoundError|FloatingPointError|FutureWarning|GeneratorExit|ImportError|ImportWarning|IndentationError|IndexError|InterruptedError|IsADirectoryError|KeyboardInterrupt|KeyError|LookupError|MemoryError|ModuleNotFoundError|NameError|NotADirectoryError|NotImplementedError|OSError|OverflowError|PendingDeprecationWarning|PermissionError|ProcessLookupError|RecursionError|ReferenceError|ResourceWarning|RuntimeError|RuntimeWarning|StopAsyncIteration|StopIteration|SyntaxError|SyntaxWarning|SystemError|SystemExit|TabError|TimeoutError|TypeError|UnboundLocalError|UnicodeDecodeError|UnicodeEncodeError|UnicodeError|UnicodeTranslateError|UnicodeWarning|UserWarning|ValueError|Warning|ZeroDivisionError)\b 0:type

# keywords (see `keyword.kwlist`)
add-highlighter shared/python/code/ regex \b(?:and|as|assert|break|class|continue|def|del|elif|else|except|exec|finally|for|from|global|if|import|in|is|lambda|not|or|pass|raise|return|try|while|with|yield)\b 0:keyword

# Operators
add-highlighter shared/python/code/ regex (?:<|>|!|=|\||\^|&|\+|-|\*|\*|//|/|%|~|:=) 0:operator

# Commands
# --------

define-command -hidden python-indent-on-new-line %{
    evaluate-commands -draft -itersel %{
        # copy '#' comment prefix and following white spaces
        try %{ execute-keys -draft k <a-x> s ^\h*#\h* <ret> y jgh P }
        # preserve previous line indent
        try %{ execute-keys -draft \; K <a-&> }
        # indent after line ending with :([
        try %{ execute-keys -draft <space> k x <a-k> [:(\[]$ <ret> j <a-gt> }
    }
}

define-command -hidden python-remove-trailing-whitespace %{
    evaluate-commands -draft %{
        # Remove trailing whitespace on all lines
        try %{ execute-keys -draft \% s \h+$ <ret> d }
    }
}

# Initialization
# --------------

hook -group python-highlight global WinSetOption filetype=python %{
    add-highlighter window/python ref python
    hook -once -always window WinSetOption filetype=.* %{ remove-highlighter window/python }
}

hook global WinSetOption filetype=python %{
    hook window InsertChar \n -group python-hooks python-indent-on-new-line
    hook window BufWritePre %val{buffile} -group python-hooks python-remove-trailing-whitespace
    hook -once -always window WinSetOption filetype=.* %{ remove-hooks window python-.+ }
}
