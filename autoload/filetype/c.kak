# C (WIP)
# ---------

# File detection
# --------------

# .c file extension
hook global BufCreate .*\.c$ %{
    set-option buffer filetype c
}

# .h is set in cpp.kak

# Hook commands
# -------------

define-command -hidden c-indent-on-new-line %<
    evaluate-commands -draft -itersel %<
        execute-keys \;
        # Copy // comment prefix
        try %< execute-keys -draft k x s ^\h*//\h* <ret> y j gh P >
        # Copy * prefix inside block comments
        try %< execute-keys -draft <a-?> /\* <ret> <a-K> \*/ <ret> <a-:> k x s ^\h*\*\h* <ret> y j gh P >
        # Preserve previous line indent
        try %< execute-keys -draft K <a-&> >
        # Indent after lines ending in { or (
        try %< execute-keys -draft k x <a-k> [({]\h*$ <ret> j <a-gt> >
        # Trim trailing whitespace from previous line
        try %< execute-keys -draft k x s \h+$ <ret> d >
    >
>

define-command -hidden c-indent-on-opening-curly-brace %<
    evaluate-commands -draft -itersel %<
        # Align indent with opening paren when { is entered on a new line after the closing paren
        try %< execute-keys -draft h <a-F> ) M <a-k> \A\(.*\)\h*\n\h*\{\z <ret> s \A|.\z <ret> 1<a-&> >
    >
>

define-command -hidden c-indent-on-closing-curly-brace %<
    evaluate-commands -draft -itersel %<
        # Align to indentation curly brace when alone on a line
        try %< execute-keys -draft x <a-k> ^\h+\}$ <ret> h <a-a> { <a-S> 1<a-&> >
    >
>

define-command -hidden c-indent-on-closing-paren %<
    evaluate-commands -draft -itersel %<
        # Same but for )
        try %< execute-keys -draft x <a-k> ^\h+\)$ <ret> h <a-a> ( <a-S> 1<a-&> >
    >
>

define-command -hidden c-remove-comment-markers %<
    evaluate-commands -draft -itersel %<
        # Remove `//` prefix when joining one comment line to the next
        try %< execute-keys -draft x <a-k> ^\h*// <ret> s '// ?' <ret> ) <a-space> d >
        # It would be nice to do the `*` prefix when in a block comment,
        # but that doesn't seem possible because the NormalKey hook is
        # called *after* the two lines are joined, so you can't really
        # tell where one line ends and the next begins
    >
>

define-command -hidden c-remove-trailing-whitespace %<
    evaluate-commands -draft %<
        # Remove trailing whitespace on all lines
        try %< execute-keys -draft \% s \h+$ <ret> d >
    >
>

# Syntax highlighting
# -------------------

add-highlighter shared/c regions
add-highlighter shared/c/code default-region group
add-highlighter shared/c/string region %{(?<!')(?<!'\\)"} %{(?<!\\)(?:\\\\)*"} fill string
add-highlighter shared/c/raw_string region %{R"([^(]*)\(} %{\)([^")]*)"} fill string
add-highlighter shared/c/comment region /\* \*/ group
add-highlighter shared/c/line_comment region // $ group
add-highlighter shared/c/disabled region -recurse "#\h*if(?:def)?" ^\h*?#\h*if\h+(?:0|FALSE)\b "#\h*(?:else|elif|endif)" fill rgb:666666
add-highlighter shared/c/macro region %{^\h*?\K#} %{(?<!\\)\n} group

add-highlighter shared/c/macro/ fill meta
add-highlighter shared/c/macro/ regex ^\h*#include\h+(\S*) 1:module

add-highlighter shared/c/comment/ fill comment
add-highlighter shared/c/comment/ regex \b(?:TODO|XXX|FIXME)\b 0:default+r
add-highlighter shared/c/line_comment/ fill comment
add-highlighter shared/c/line_comment/ regex \b(?:TODO|XXX|FIXME)\b 0:default+r

# integer literals
add-highlighter shared/c/code/ regex %{(?i)(?<!\.)\b[1-9]('?\d+)*(ul?l?|ll?u?)?\b(?!\.)} 0:value
add-highlighter shared/c/code/ regex %{(?i)(?<!\.)\b0b[01]('?[01]+)*(ul?l?|ll?u?)?\b(?!\.)} 0:value
add-highlighter shared/c/code/ regex %{(?i)(?<!\.)\b0('?[0-7]+)*(ul?l?|ll?u?)?\b(?!\.)} 0:value
add-highlighter shared/c/code/ regex %{(?i)(?<!\.)\b0x[\da-f]('?[\da-f]+)*(ul?l?|ll?u?)?\b(?!\.)} 0:value

# operators
add-highlighter shared/c/code/ regex [=&|<>?+\-/%!~*\^]|:: 0:operator

# floating point literals
add-highlighter shared/c/code/ regex %{(?i)(?<!\.)\b\d('?\d+)*\.([fl]\b|\B)(?!\.)} 0:value
add-highlighter shared/c/code/ regex %{(?i)(?<!\.)\b\d('?\d+)*\.?e[+-]?\d('?\d+)*[fl]?\b(?!\.)} 0:value
add-highlighter shared/c/code/ regex %{(?i)(?<!\.)(\b(\d('?\d+)*)|\B)\.\d('?[\d]+)*(e[+-]?\d('?\d+)*)?[fl]?\b(?!\.)} 0:value
add-highlighter shared/c/code/ regex %{(?i)(?<!\.)\b0x[\da-f]('?[\da-f]+)*\.([fl]\b|\B)(?!\.)} 0:value
add-highlighter shared/c/code/ regex %{(?i)(?<!\.)\b0x[\da-f]('?[\da-f]+)*\.?p[+-]?\d('?\d+)*)?[fl]?\b(?!\.)} 0:value
add-highlighter shared/c/code/ regex %{(?i)(?<!\.)\b0x([\da-f]('?[\da-f]+)*)?\.\d('?[\d]+)*(p[+-]?\d('?\d+)*)?[fl]?\b(?!\.)} 0:value

# character literals (no multi-character literals)
add-highlighter shared/c/code/ regex %{(\b(u8|u|U|L)|\B)'((\\.)|[^'\\])'\B} 0:value

# Highlight functions
add-highlighter shared/c/code/ regex \b\w+\s*(?=\() 0:function

# Keywords
add-highlighter shared/c/code/ regex \b(auto|break|case|continue|default|do|else|enum|for|goto|if|inline|return|sizeof|struct|switch|typedef|union|while)\b 0:keyword
add-highlighter shared/c/code/ regex \b(_Alignas|_Alignof|_Generic|_Noreturn|_Static_assert|_Thread_local|alignas|alignof|noreturn|static_assert|thread_local)\b 0:keyword
# Attributes
add-highlighter shared/c/code/ regex \b(const|extern|mutable|register|restrict|static|volatile)\b 0:attribute
# Types
add-highlighter shared/c/code/ regex \b(char|double|float|int|long|short|signed|unsigned|void)\b 0:type
## <stddef.h>
add-highlighter shared/c/code/ regex \b(max_align_t|ptrdiff_t|size_t)\b 0:type
## <stdbool.h>
add-highlighter shared/c/code/ regex \b(bool|_Bool)\b 0:type
## <stdint.h>
add-highlighter shared/c/code/ regex \b(int8_t|int16_t|int32_t|int64_t|intmax_t|intptr_t)\b 0:type
add-highlighter shared/c/code/ regex \b(uint8_t|uint16_t|uint32_t|uint64_t|uintmax_t|uintptr_t)\b 0:type
## <stdatomic.h>
add-highlighter shared/c/code/ regex \b(memory_order|atomic_flag)\b 0:type
add-highlighter shared/c/code/ regex \batomic_(bool|char|schar|uchar|short|ushort|int|uint|long|ulong|llong|ullong|intptr_t|uintptr_t|size_t|ptrdiff_t|intmax_t|uintmax_t)\b 0:type
# Values
add-highlighter shared/c/code/ regex \b(NULL|false|true)\b 0:value
# Compiler macros
add-highlighter shared/c/code/ regex \b(__cplusplus|__STDC_HOSTED__|__FILE__|__LINE__|__DATE__|__TIME__|__STDCPP_DEFAULT_NEW_ALIGNMENT__)\b 0:builtin

# Initialization
# ~~~~~~~~~~~~~~

hook -group c-highlight global WinSetOption filetype=c %{
    add-highlighter window/c ref c
    hook -once -always window WinSetOption filetype=.* %{ remove-highlighter window/c }
}

hook global WinSetOption filetype=c %[
    hook window InsertChar \n -group c-hooks c-indent-on-new-line
    hook window InsertChar \{ -group c-hooks c-indent-on-opening-curly-brace
    hook window InsertChar \} -group c-hooks c-indent-on-closing-curly-brace
    hook window InsertChar \) -group c-hooks c-indent-on-closing-paren
    hook window NormalKey <a-j> -group c-hooks c-remove-comment-markers
    hook window NormalKey <a-J> -group c-hooks c-remove-comment-markers
    # TODO: %val{buffile} needs to be escaped to use as a regex
    hook window BufWritePre %val{buffile} -group c-hooks c-remove-trailing-whitespace
    hook -once -always window WinSetOption filetype=.* %{ remove-hooks window c-.+ }
]
