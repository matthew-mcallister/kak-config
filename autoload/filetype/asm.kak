# x86 assembly
# ------------

# File detection
# --------------

# .s file extension
hook global BufCreate .*\.s$ %{
    set-option buffer filetype asm
}

# Hook commands
# -------------

define-command -hidden asm-indent-on-new-line %<
    evaluate-commands -draft -itersel %<
        execute-keys \;
        # Copy comment prefix
        try %< execute-keys -draft k x s ^\h*[#;]\h* <ret> y j gh P >
        # Preserve previous line indent
        try %< execute-keys -draft K <a-&> >
        # Indent after labels
        try %< execute-keys -draft k x <a-k> ^\h*[.a-zA-Z0-9]+\h*$ <ret> j <a-gt> >
    >
>

define-command -hidden asm-remove-comment-markers %<
    evaluate-commands -draft -itersel %<
        # Remove comment prefix when joining one comment line to the next
        try %< execute-keys -draft <a-l> s \A\h*[#;]\h* <ret> c ' ' >
    >
>

define-command -hidden asm-remove-trailing-whitespace %[
    evaluate-commands -draft %_
        # Remove trailing whitespace on all lines
        try %[ execute-keys -draft \% s \h+$ <ret> d ]
    _
]

# Syntax highlighting
# -------------------

add-highlighter shared/asm regions
add-highlighter shared/asm/code default-region group
add-highlighter shared/asm/string region %{(?<!')(?<!'\\)"} %{(?<!\\)(?:\\\\)*"} fill string
add-highlighter shared/asm/line_comment region '[#;]' '$' group

add-highlighter shared/asm/line_comment/ fill comment
add-highlighter shared/asm/line_comment/ regex \b(?:TODO|XXX|FIXME)\b 0:default+r

# Operators
add-highlighter shared/asm/code/ regex [=&|?+\-/%!~*\^#<>] 0:operator

# Integer literals
add-highlighter shared/asm/code/ regex %{\b[1-9]('?\d+)*\b} 0:value
add-highlighter shared/asm/code/ regex %{\b0('?[0-7]+)*\b} 0:value
add-highlighter shared/asm/code/ regex %{(?i)\b0x[\da-f]('?[\da-f]+)*\b} 0:value

# Labels
add-highlighter shared/asm/code/ regex ^\h*([A-Za-z0-9_]+): 1:function
# Meta/macros
add-highlighter shared/asm/code/ regex \.[a-zA-Z0-9_\.-]+ 0:meta
# Special args
add-highlighter shared/asm/code/ regex @[a-zA-Z0-9_]+\b 0:attribute

# Instructions (TODO: GAS suffixes are hacked in)
add-highlighter shared/asm/code/ regex (?i)\b(?:aaa|aad|aam|aas|adc|adcx|add|addpd|addps|addsd|addss|addsubpd|addsubps|adox|aesdec|aesdeclast|aesenc|aesenclast|aesimc|aeskeygenassist|and|andn|andnpd|andnps|andpd|andps|arpl|bextr|blendpd|blendps|blendvpd|blendvps|blsi|blsmsk|blsr|bndcl|bndcn|bndcu|bndldx|bndmk|bndmov|bndstx|bound|bsf|bsr|bswap|bt|btc|btr|bts|bzhi|call|cbw|cdq|cdqe|clac|clc|cld|cldemote|clflush|clflushopt|cli|clts|clwb|cmc|cmovcc|cmp|cmppd|cmpps|cmps|cmpsb|cmpsd|cmpsq|cmpss|cmpsw|cmpxchg|cmpxchg16b|cmpxchg8b|comisd|comiss|cpuid|cqo|crc32|cvtdq2pd|cvtdq2ps|cvtpd2dq|cvtpd2pi|cvtpd2ps|cvtpi2pd|cvtpi2ps|cvtps2dq|cvtps2pd|cvtps2pi|cvtsd2si|cvtsd2ss|cvtsi2sd|cvtsi2ss|cvtss2sd|cvtss2si|cvttpd2dq|cvttpd2pi|cvttps2dq|cvttps2pi|cvttsd2si|cvttss2si|cwd|cwde|daa|das|dec|div|divpd|divps|divsd|divss|dppd|dpps|emms|enter|extractps|f2xm1|fabs|fadd|faddp|fbld|fbstp|fchs|fclex|fcmovcc|fcom|fcomi|fcomip|fcomp|fcompp|fcos|fdecstp|fdiv|fdivp|fdivr|fdivrp|ffree|fiadd|ficom|ficomp|fidiv|fidivr|fild|fimul|fincstp|finit|fist|fistp|fisttp|fisub|fisubr|fld|fld1|fldcw|fldenv|fldl2e|fldl2t|fldlg2|fldln2|fldpi|fldz|fmul|fmulp|fnclex|fninit|fnop|fnsave|fnstcw|fnstenv|fnstsw|fpatan|fprem|fprem1|fptan|frndint|frstor|fsave|fscale|fsin|fsincos|fsqrt|fst|fstcw|fstenv|fstp|fstsw|fsub|fsubp|fsubr|fsubrp|ftst|fucom|fucomi|fucomip|fucomp|fucompp|fwait|fxam|fxch|fxrstor|fxsave|fxtract|fyl2x|fyl2xp1|gf2p8affineinvqb|gf2p8affineqb|gf2p8mulb|haddpd|haddps|hlt|hsubpd|hsubps|idiv|imul|in|inc|ins|insb|insd|insertps|insw|int|int1|int3|into|invd|invlpg|invpcid|iret|iretd|jmp|ja|jae|jb|jbe|jc|jcxz|jecxz|je|jg|jge|jl|jle|jna|jnae|jnb|jnbe|jnc|jne|jng|jnge|jnl|jnle|jno|jnp|jns|jnz|jo|jp|js|jz|jcc|kaddb|kaddd|kaddq|kaddw|kandb|kandd|kandnb|kandnd|kandnq|kandnw|kandq|kandw|kmovb|kmovd|kmovq|kmovw|knotb|knotd|knotq|knotw|korb|kord|korq|kortestb|kortestd|kortestq|kortestw|korw|kshiftlb|kshiftld|kshiftlq|kshiftlw|kshiftrb|kshiftrd|kshiftrq|kshiftrw|ktestb|ktestd|ktestq|ktestw|kunpckbw|kunpckdq|kunpckwd|kxnorb|kxnord|kxnorq|kxnorw|kxorb|kxord|kxorq|kxorw|lahf|lar|lddqu|ldmxcsr|lds|lea|leave|les|lfence|lfs|lgdt|lgs|lidt|lldt|lmsw|lock|lods|lodsb|lodsd|lodsq|lodsw|loop|loopcc|lsl|lss|ltr|lzcnt|maskmovdqu|maskmovq|maxpd|maxps|maxsd|maxss|mfence|minpd|minps|minsd|minss|monitor|mov|movapd|movaps|movbe|movd|movddup|movdir64b|movdiri|movdq2q|movdqa|movdqu|movhlps|movhpd|movhps|movlhps|movlpd|movlps|movmskpd|movmskps|movntdq|movntdqa|movnti|movntpd|movntps|movntq|movq|movq2dq|movs|movsb|movsd|movshdup|movsldup|movsq|movss|movsw|movsx|movsxd|movupd|movups|movzx|mpsadbw|mul|mulpd|mulps|mulsd|mulss|mulx|mwait|neg|nop|not|or|orpd|orps|out|outs|outsb|outsd|outsw|pabsb|pabsd|pabsq|pabsw|packssdw|packsswb|packusdw|packuswb|paddb|paddd|paddq|paddsb|paddsw|paddusb|paddusw|paddw|palignr|pand|pandn|pause|pavgb|pavgw|pblendvb|pblendw|pclmulqdq|pcmpeqb|pcmpeqd|pcmpeqq|pcmpeqw|pcmpestri|pcmpestrm|pcmpgtb|pcmpgtd|pcmpgtq|pcmpgtw|pcmpistri|pcmpistrm|pdep|pext|pextrb|pextrd|pextrq|pextrw|phaddd|phaddsw|phaddw|phminposuw|phsubd|phsubsw|phsubw|pinsrb|pinsrd|pinsrq|pinsrw|pmaddubsw|pmaddwd|pmaxsb|pmaxsd|pmaxsq|pmaxsw|pmaxub|pmaxud|pmaxuq|pmaxuw|pminsb|pminsd|pminsq|pminsw|pminub|pminud|pminuq|pminuw|pmovmskb|pmovsx|pmovzx|pmuldq|pmulhrsw|pmulhuw|pmulhw|pmulld|pmullq|pmullw|pmuludq|pop|popa|popad|popcnt|popf|popfd|popfq|por|prefetchw|prefetchh|psadbw|pshufb|pshufd|pshufhw|pshuflw|pshufw|psignb|psignd|psignw|pslld|pslldq|psllq|psllw|psrad|psraq|psraw|psrld|psrldq|psrlq|psrlw|psubb|psubd|psubq|psubsb|psubsw|psubusb|psubusw|psubw|ptest|ptwrite|punpckhbw|punpckhdq|punpckhqdq|punpckhwd|punpcklbw|punpckldq|punpcklqdq|punpcklwd|push|pusha|pushad|pushf|pushfd|pushfq|pxor|rcl|rcpps|rcpss|rcr|rdfsbase|rdgsbase|rdmsr|rdpid|rdpkru|rdpmc|rdrand|rdseed|rdtsc|rdtscp|rep|repe|repne|repnz|repz|ret|rol|ror|rorx|roundpd|roundps|roundsd|roundss|rsm|rsqrtps|rsqrtss|sahf|sal|sar|sarx|sbb|scas|scasb|scasd|scasw|setcc|sfence|sgdt|sha1msg1|sha1msg2|sha1nexte|sha1rnds4|sha256msg1|sha256msg2|sha256rnds2|shl|shld|shlx|shr|shrd|shrx|shufpd|shufps|sidt|sldt|smsw|sqrtpd|sqrtps|sqrtsd|sqrtss|stac|stc|std|sti|stmxcsr|stos|stosb|stosd|stosq|stosw|str|sub|subpd|subps|subsd|subss|swapgs|syscall|sysenter|sysexit|sysret|test|tpause|tzcnt|ucomisd|ucomiss|ud|umonitor|umwait|unpckhpd|unpckhps|unpcklpd|unpcklps|valignd|valignq|vblendmpd|vblendmps|vbroadcast|vcompresspd|vcompressps|vcvtpd2qq|vcvtpd2udq|vcvtpd2uqq|vcvtph2ps|vcvtps2ph|vcvtps2qq|vcvtps2udq|vcvtps2uqq|vcvtqq2pd|vcvtqq2ps|vcvtsd2usi|vcvtss2usi|vcvttpd2qq|vcvttpd2udq|vcvttpd2uqq|vcvttps2qq|vcvttps2udq|vcvttps2uqq|vcvttsd2usi|vcvttss2usi|vcvtudq2pd|vcvtudq2ps|vcvtuqq2pd|vcvtuqq2ps|vcvtusi2sd|vcvtusi2ss|vdbpsadbw|verr|verw|vexpandpd|vexpandps|vextractf128|vextractf32x4|vextractf32x8|vextractf64x2|vextractf64x4|vextracti128|vextracti32x4|vextracti32x8|vextracti64x2|vextracti64x4|vfixupimmpd|vfixupimmps|vfixupimmsd|vfixupimmss|vfmadd132pd|vfmadd132ps|vfmadd132sd|vfmadd132ss|vfmadd213pd|vfmadd213ps|vfmadd213sd|vfmadd213ss|vfmadd231pd|vfmadd231ps|vfmadd231sd|vfmadd231ss|vfmaddsub132pd|vfmaddsub132ps|vfmaddsub213pd|vfmaddsub213ps|vfmaddsub231pd|vfmaddsub231ps|vfmsub132pd|vfmsub132ps|vfmsub132sd|vfmsub132ss|vfmsub213pd|vfmsub213ps|vfmsub213sd|vfmsub213ss|vfmsub231pd|vfmsub231ps|vfmsub231sd|vfmsub231ss|vfmsubadd132pd|vfmsubadd132ps|vfmsubadd213pd|vfmsubadd213ps|vfmsubadd231pd|vfmsubadd231ps|vfnmadd132pd|vfnmadd132ps|vfnmadd132sd|vfnmadd132ss|vfnmadd213pd|vfnmadd213ps|vfnmadd213sd|vfnmadd213ss|vfnmadd231pd|vfnmadd231ps|vfnmadd231sd|vfnmadd231ss|vfnmsub132pd|vfnmsub132ps|vfnmsub132sd|vfnmsub132ss|vfnmsub213pd|vfnmsub213ps|vfnmsub213sd|vfnmsub213ss|vfnmsub231pd|vfnmsub231ps|vfnmsub231sd|vfnmsub231ss|vfpclasspd|vfpclassps|vfpclasssd|vfpclassss|vgatherdpd|vgatherdps|vgatherqpd|vgatherqps|vgetexppd|vgetexpps|vgetexpsd|vgetexpss|vgetmantpd|vgetmantps|vgetmantsd|vgetmantss|vinsertf128|vinsertf32x4|vinsertf32x8|vinsertf64x2|vinsertf64x4|vinserti128|vinserti32x4|vinserti32x8|vinserti64x2|vinserti64x4|vmaskmov|vmovdqa32|vmovdqa64|vmovdqu16|vmovdqu32|vmovdqu64|vmovdqu8|vpblendd|vpblendmb|vpblendmd|vpblendmq|vpblendmw|vpbroadcast|vpbroadcastb|vpbroadcastd|vpbroadcastm|vpbroadcastq|vpbroadcastw|vpcmpb|vpcmpd|vpcmpq|vpcmpub|vpcmpud|vpcmpuq|vpcmpuw|vpcmpw|vpcompressd|vpcompressq|vpconflictd|vpconflictq|vperm2f128|vperm2i128|vpermb|vpermd|vpermi2b|vpermi2d|vpermi2pd|vpermi2ps|vpermi2q|vpermi2w|vpermilpd|vpermilps|vpermpd|vpermps|vpermq|vpermt2b|vpermt2d|vpermt2pd|vpermt2ps|vpermt2q|vpermt2w|vpermw|vpexpandd|vpexpandq|vpgatherdd|vpgatherdq|vpgatherqd|vpgatherqq|vplzcntd|vplzcntq|vpmadd52huq|vpmadd52luq|vpmaskmov|vpmovb2m|vpmovd2m|vpmovdb|vpmovdw|vpmovm2b|vpmovm2d|vpmovm2q|vpmovm2w|vpmovq2m|vpmovqb|vpmovqd|vpmovqw|vpmovsdb|vpmovsdw|vpmovsqb|vpmovsqd|vpmovsqw|vpmovswb|vpmovusdb|vpmovusdw|vpmovusqb|vpmovusqd|vpmovusqw|vpmovuswb|vpmovw2m|vpmovwb|vpmultishiftqb|vprold|vprolq|vprolvd|vprolvq|vprord|vprorq|vprorvd|vprorvq|vpscatterdd|vpscatterdq|vpscatterqd|vpscatterqq|vpsllvd|vpsllvq|vpsllvw|vpsravd|vpsravq|vpsravw|vpsrlvd|vpsrlvq|vpsrlvw|vpternlogd|vpternlogq|vptestmb|vptestmd|vptestmq|vptestmw|vptestnmb|vptestnmd|vptestnmq|vptestnmw|vrangepd|vrangeps|vrangesd|vrangess|vrcp14pd|vrcp14ps|vrcp14sd|vrcp14ss|vreducepd|vreduceps|vreducesd|vreducess|vrndscalepd|vrndscaleps|vrndscalesd|vrndscaless|vrsqrt14pd|vrsqrt14ps|vrsqrt14sd|vrsqrt14ss|vscalefpd|vscalefps|vscalefsd|vscalefss|vscatterdpd|vscatterdps|vscatterqpd|vscatterqps|vshuff32x4|vshuff64x2|vshufi32x4|vshufi64x2|vtestpd|vtestps|vzeroall|vzeroupper|wait|wbinvd|wrfsbase|wrgsbase|wrmsr|wrpkru|xabort|xacquire|xadd|xbegin|xchg|xend|xgetbv|xlat|xlatb|xor|xorpd|xorps|xrelease|xrstor|xrstors|xsave|xsavec|xsaveopt|xsaves|xsetbv|xtest|invept|invvpid|vmcall|vmclear|vmfunc|vmlaunch|vmptrld|vmptrst|vmread|vmresume|vmwrite|vmxoff|vmxon|prefetchwt1|v4fmaddps|v4fmaddss|v4fnmaddps|v4fnmaddss|vexp2pd|vexp2ps|vgatherpf0dpd|vgatherpf0dps|vgatherpf0qpd|vgatherpf0qps|vgatherpf1dpd|vgatherpf1dps|vgatherpf1qpd|vgatherpf1qps|vp4dpwssd|vp4dpwssds|vrcp28pd|vrcp28ps|vrcp28sd|vrcp28ss|vrsqrt28pd|vrsqrt28ps|vrsqrt28sd|vrsqrt28ss|vscatterpf0dpd|vscatterpf0dps|vscatterpf0qpd|vscatterpf0qps|vscatterpf1dpd|vscatterpf1dps|vscatterpf1qpd|vscatterpf1qps)[bswlqt]?\b 0:keyword
# Registers
add-highlighter shared/asm/code/ regex (?:%|\b)(?:rax|rcx|rdx|rbx|rsp|rbp|rsi|rdi|eax|ecx|edx|ebx|esp|ebp|esi|edi|ax|cx|dx|bx|sp|bp|si|di|ah|al|ch|cl|dh|dl|bh|bl|spl|bpl|sil|dil|r8|r8d|r8w|r8b|r9|r9d|r9w|r9b|r10|r10d|r10w|r10b|r11|r11d|r11w|r11b|r12|r12d|r12w|r12b|r13|r13d|r13w|r13b|r14|r14d|r14w|r14b|r15|r15d|r15w|r15b|xmm0|xmm1|xmm2|xmm3|xmm4|xmm5|xmm6|xmm7|ymm0|ymm1|ymm2|ymm3|ymm4|ymm5|ymm6|ymm7|zmm0|zmm1|zmm2|zmm3|zmm4|zmm5|zmm6|zmm7)\b 0:value

# TODO: Incomplete flavor highlighting
# Intel syntax
add-highlighter shared/asm/code/ regex \b(?:byte|word|dword|qword|xmmword|ymmword|zmmword|ptr)\b 0:attribute
# GAS syntax

# Initialization
# ~~~~~~~~~~~~~~

hook -group asm-highlight global WinSetOption filetype=asm %{
    add-highlighter window/asm ref asm
    hook -once -always window WinSetOption filetype=.* %{ remove-highlighter window/a
sm}
}

hook global WinSetOption filetype=asm %[
    hook window InsertChar \n -group asm-hooks asm-indent-on-new-line
    hook window NormalKey <a-j> -group asm-hooks asm-remove-comment-markers
    hook window NormalKey <a-J> -group asm-hooks asm-remove-comment-markers
    # TODO: %val{buffile} needs to be escaped to use as a regex
    hook window BufWritePre %val{buffile} -group asm-hooks asm-remove-trailing-whitespace
    hook -once -always window WinSetOption filetype=.* %{ remove-hooks windowasm-.+ }
]
