# Detection
# ‾‾‾‾‾‾‾‾‾

hook global BufCreate .*[.](js|jsx|mjs) %{
    set-option buffer filetype javascript
}

# Commands
# ‾‾‾‾‾‾‾‾

define-command -hidden javascript-indent-on-new-line %<
    evaluate-commands -draft -itersel %<
        execute-keys \;
        # Copy // comments prefix and following whitespace
        try %< execute-keys -draft k x s '^\h*\K//\h*' <ret> y gh j P >
        # Preserve previous line indent
        try %< execute-keys -draft K <a-&> >
        # Indent after lines ending with {, (, or [
        try %< execute-keys -draft k x <a-k> '[{(\[]\h*$' <ret> j <a-gt> >
        # Trim trailing whitespace
        try %< execute-keys -draft k x s \h+$ <ret> d >
    >
>

define-command -hidden javascript-remove-trailing-whitespace %[
    evaluate-commands -draft %_
        # Remove trailing whitespace on all lines
        try %[ execute-keys -draft \% s \h+$ <ret> d ]
    _
]

# Highlighting and hooks bulder for JavaScript and TypeScript
# ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾
define-command -hidden init-javascript-filetype -params 1 %~
    # Highlighters
    # ‾‾‾‾‾‾‾‾‾‾‾‾

    add-highlighter "shared/%arg{1}" regions
    add-highlighter "shared/%arg{1}/code" default-region group
    add-highlighter "shared/%arg{1}/double_string" region '"'  (?<!\\)(\\\\)*"         fill string
    add-highlighter "shared/%arg{1}/single_string" region "'"  (?<!\\)(\\\\)*'         fill string
    add-highlighter "shared/%arg{1}/literal"       region "`"  (?<!\\)(\\\\)*`         group
    add-highlighter "shared/%arg{1}/comment_line"  region //   '$'                     fill comment
    add-highlighter "shared/%arg{1}/shebang"       region ^#!  $                       fill meta
    add-highlighter "shared/%arg{1}/comment"       region /\*  \*/                     fill comment
    add-highlighter "shared/%arg{1}/jsx"           region -recurse (?<![\w<])<[a-zA-Z][\w:.-]* (?<![\w<])<[a-zA-Z][\w:.-]*(?!\hextends)(?=[\s/>])(?!>\()) (</.*?>|/>) regions

    # Regular expression flags are: g → global match, i → ignore case, m → multi-lines, u → unicode, y → sticky
    # https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp

    add-highlighter "shared/%arg{1}/literal/"       fill string
    add-highlighter "shared/%arg{1}/literal/"       regex \$\{.*?\} 0:value

    add-highlighter "shared/%arg{1}/code/" regex %{[=&|<>?+\-/!*\^%%~~]} 0:operator
    add-highlighter "shared/%arg{1}/code/" regex [^$_]\b(document|false|null|parent|self|this|true|undefined|window)\b 1:value
    add-highlighter "shared/%arg{1}/code/" regex "\b([1-9][0-9]*)?\.?[0-9]+" 0:value
    add-highlighter "shared/%arg{1}/code/" regex "\b0x[0-9a-fA-F]+" 0:value
    add-highlighter "shared/%arg{1}/code/" regex "\b0[0-7]+" 0:value
    add-highlighter "shared/%arg{1}/code/" regex "\b0b[01]+" 0:value
    add-highlighter "shared/%arg{1}/code/" regex \b(Array|Boolean|Date|Function|Number|Object|RegExp|String|Symbol)\b 0:type

    add-highlighter "shared/%arg{1}/code/" regex "\b(\w[\w\d]*)\s*\(" 1:value

    # jsx: In well-formed xml the number of opening and closing tags match up regardless of tag name.
    #
    # We inline a small XML highlighter here since it anyway need to recurse back up to the starting highlighter.
    # To make things simple we assume that jsx is always enabled.

    add-highlighter "shared/%arg{1}/jsx/tag"  region -recurse <  <(?=[/a-zA-Z]) (?<!=)> regions
    add-highlighter "shared/%arg{1}/jsx/expr" region -recurse \{ \{             \}      ref %arg{1}

    add-highlighter "shared/%arg{1}/jsx/tag/base" default-region group
    add-highlighter "shared/%arg{1}/jsx/tag/double_string" region =\K" (?<!\\)(\\\\)*" fill string
    add-highlighter "shared/%arg{1}/jsx/tag/single_string" region =\K' (?<!\\)(\\\\)*' fill string
    add-highlighter "shared/%arg{1}/jsx/tag/expr" region -recurse \{ \{   \}           group

    add-highlighter "shared/%arg{1}/jsx/tag/base/" regex (\w+) 1:attribute
    add-highlighter "shared/%arg{1}/jsx/tag/base/" regex </?([\w-$]+) 1:keyword
    add-highlighter "shared/%arg{1}/jsx/tag/base/" regex (</?|/?>) 0:meta

    add-highlighter "shared/%arg{1}/jsx/tag/expr/"   fill default,default+F
    add-highlighter "shared/%arg{1}/jsx/tag/expr/"   ref %arg{1}

    # Keywords are collected at
    # https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Lexical_grammar#Keywords
    add-highlighter "shared/%arg{1}/code/" regex \b(async|await|break|case|catch|class|const|constructor|continue|debugger|default|delete|do|else|export|extends|finally|for|from|function|get|if|import|in|instanceof|let|new|of|return|set|static|super|switch|throw|try|typeof|var|void|while|with|yield)\b 0:keyword
    add-highlighter "shared/%arg{1}/code/" regex \b(async|function|yield)\* 0:keyword

    # Initialization
    # ‾‾‾‾‾‾‾‾‾‾‾‾‾‾

    hook -group "%arg{1}-highlight" global WinSetOption "filetype=%arg{1}" "
        add-highlighter window/%arg{1} ref %arg{1}

        hook -once -always window WinSetOption filetype=.* %%{ remove-highlighter window/%arg{1} }
    "

    hook global WinSetOption "filetype=%arg{1}" "
        hook global WinSetOption filetype=%arg{1} %%[
            hook window InsertChar \n -group %arg{1}-hooks javascript-indent-on-new-line
            hook window BufWritePre %%val{buffile} -group %arg{1}-hooks javascript-remove-trailing-whitespace

            hook -once -always window WinSetOption filetype=.* %%{ remove-hooks window %arg{1}-.+ }
        ]
    "
~

init-javascript-filetype javascript
init-javascript-filetype typescript

# Highlighting specific to TypeScript
# ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾
add-highlighter shared/typescript/code/ regex \b(array|boolean|date|number|object|regexp|string|symbol)\b 0:type

# Keywords grabbed from https://github.com/Microsoft/TypeScript/issues/2536
add-highlighter shared/typescript/code/ regex \b(as|constructor|declare|enum|from|implements|interface|module|namespace|package|private|protected|public|readonly|static|type)\b 0:keyword

hook global WinSetOption filetype=javascript %[
    hook window InsertChar \n -group javascript-hooks javascript-indent-on-new-line
    hook window BufWritePre %val{buffile} -group javascript-hooks javascript-remove-trailing-whitespace
    hook -once -always window WinSetOption filetype=.* %{ remove-hooks window javascript-.+ }
]
