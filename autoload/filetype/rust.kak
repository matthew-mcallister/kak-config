# http://rust-lang.org
# ~~~~~~~~~~~~~~~~~~~~

# Detection
# ~~~~~~~~~

hook global BufCreate .*\.rs %{
    set-option buffer filetype rust
}

# Highlighters
# ~~~~~~~~~~~~

add-highlighter shared/rust regions
add-highlighter shared/rust/code default-region group

add-highlighter shared/rust/string region %{(?<!')b?r?"} %{(?<!\\)(\\\\)*"} fill string
add-highlighter shared/rust/raw_string region %{r#"} %{"#} fill string

add-highlighter shared/rust/line_comment region // $ group
add-highlighter shared/rust/line_comment/ regex \b(?:TODO|XXX|FIXME)\b 0:default+r
add-highlighter shared/rust/line_comment/ fill comment

add-highlighter shared/rust/nested_comment region -recurse /\* /\* \*/ group
add-highlighter shared/rust/nested_comment/ regex \b(?:TODO|XXX|FIXME)\b 0:default+r
add-highlighter shared/rust/nested_comment/ fill comment

add-highlighter shared/rust/attr region -recurse \[ %{#!?\[} \] group
add-highlighter shared/rust/attr/ fill meta
add-highlighter shared/rust/attr/ regex %{".*?(?<!\\)(\\\\)*"} 0:string

# Hack to highlight $( and )* in macros
# TODO: This should work but doesn't: $($())
add-highlighter shared/rust/macro region -recurse \( \$\( %{\)[,;]?[*+?]?} ref rust-macro
add-highlighter shared/rust-macro regions
add-highlighter shared/rust-macro/default default-region fill meta
add-highlighter shared/rust-macro/inner region -recurse \( \$\(\K (?=\)) ref rust

# Qualified names
add-highlighter shared/rust/code/ regex \b\w+(?=::) 0:meta

# Function declaration/invocation
add-highlighter shared/rust/code/ regex \bfn\s+(\w+) 1:function
add-highlighter shared/rust/code/ regex \b(\w+)\s*(?:\(|::\s*<) 1:function

# Literals and operators
add-highlighter shared/rust/code/ regex [=&|<>?+\-/%!~*\^]|:: 0:operator
add-highlighter shared/rust/code/ regex \b(?:self|true|false|None|(?:0x[_0-9a-fA-F]+|0o[_0-7]+|0b[_01]+|[0-9][_0-9]*)(?:[iu](?:8|16|32|64|128|size)|f32|f64)?|[0-9][_0-9]*(?:\.[0-9][_0-9]*|(?:\.[0-9][_0-9]*)?[eE][\+\-]?[_0-9]+)(?:f32|f64)?)\b 0:value

# Keywords
add-highlighter shared/rust/code/ regex \b(?:await|super|crate|use|extern|let|as|fn|return|do|catch|try|match|if|else|loop|for|in|while|break|continue|where|impl|dyn|pub|async|mod|trait|struct|union|enum|type)\b 0:keyword
add-highlighter shared/rust/code/ regex \b(?:async|mut|ref|static|const|move|box)\b 0:attribute
add-highlighter shared/rust/code/ regex \bunsafe\b 0:+u@attribute
add-highlighter shared/rust/code/ regex \b(?:u8|u16|u32|u64|u128|usize|i8|i16|i32|i64|i128|isize|f32|f64|bool|char|str|Self)\b 0:type
# Contextually a keyword
add-highlighter shared/rust/code/ regex &\s*(raw)\b 1:keyword

# Char literals/lifetimes
add-highlighter shared/rust/code/ regex %{(?:\bb)?'(?:[^'\\]|\\.[^']*)'} 0:value
add-highlighter shared/rust/code/ regex %{'\w+} 0:value

# Macro invocations
add-highlighter shared/rust/code/ regex \b\w+! 0:meta

# Macro variables
add-highlighter shared/rust/code/ regex \$\w+ 0:type

# Types/traits included by prelude
add-highlighter shared/rust/code/ regex \b(?:Copy|Send|Sized|Sync|Drop|Fn|FnMut|FnOnce|Box|ToOwned|Clone|PartialEq|PartialOrd|Eq|Ord|AsRef|AsMut|Into|From|FromIterator|Default|Iterator|Extend|IntoIterator|DoubleEndedIterator|ExactSizeIterator|Option|Result|SliceConcatExt|String|ToString|TryFrom|TryInto|Vec)\b 0:type

# Commands
# ~~~~~~~~

define-command -hidden rust-indent-on-new-line %~
    evaluate-commands -draft -itersel %_
        execute-keys \;
        # Copy // comments prefix and following whitespace
        try %< execute-keys -draft k x s '^\h*\K//[!/]?\h*' <ret> y gh j P >
        # Preserve previous line indent
        try %< execute-keys -draft K <a-&> >
        # Indent after lines ending with {, (, or [
        try %< execute-keys -draft k x <a-k> '[{(\[]\h*$' <ret> j <a-gt> >
        # Indent after lines ending with "where" or "->"
        try %{ execute-keys -draft k x <a-k> '(\bwhere|->)\h*$' <ret> j <a-gt> }
        # Trim trailing whitespace
        try %< execute-keys -draft k x s \h+$ <ret> d >
    _
~

define-command -hidden rust-indent-on-opening-curly-brace %[
    evaluate-commands -draft -itersel %_
        # align indent with opening paren when { is entered on a new line after the closing paren
        try %[ execute-keys -draft h <a-F> ) M <a-k> \A\(.*\)\h*\n\h*\{\z <ret> s \A|.\z <ret> 1<a-&> ]
    _
]

define-command -hidden rust-indent-on-closing-curly-brace %[
    evaluate-commands -draft -itersel %_
        # Align to indentation curly brace when alone on a line
        try %[ execute-keys -draft x <a-k> ^\h+\}$ <ret> h <a-a> { <a-S> 1<a-&> ]
    _
]

define-command -hidden rust-indent-on-closing-paren %[
    evaluate-commands -draft -itersel %_
        # Same but for )
        try %[ execute-keys -draft x <a-k> ^\h+\)$ <ret> h <a-a> ( <a-S> 1<a-&> ]
    _
]

define-command -hidden rust-indent-on-closing-square %(
    evaluate-commands -draft -itersel %_
        # Same but for ]
        try %{ execute-keys -draft x <a-k> ^\h+\]$ <ret> h <a-a> [ <a-S> 1<a-&> }
    _
)

define-command -hidden rust-remove-comment-markers %[
    evaluate-commands -draft -itersel %_
        # Remove `//` prefix when joining one comment line to the next
        try %[ execute-keys -draft <a-l> s '\A\h*//[!/]?\h*' <ret> c ' ' ]
    _
]

define-command -hidden rust-remove-trailing-whitespace %[
    evaluate-commands -draft %_
        # Remove trailing whitespace on all lines
        try %[ execute-keys -draft \% s \h+$ <ret> d ]
    _
]

# Initialization
# ~~~~~~~~~~~~~~

hook -group rust-highlight global WinSetOption filetype=rust %{
    add-highlighter window/rust ref rust
    hook -once -always window WinSetOption filetype=.* %{ remove-highlighter window/rust }
}

hook global WinSetOption filetype=rust %[
    hook window InsertChar \n -group rust-hooks rust-indent-on-new-line
    hook window InsertChar \{ -group rust-hooks rust-indent-on-opening-curly-brace
    hook window InsertChar \} -group rust-hooks rust-indent-on-closing-curly-brace
    hook window InsertChar \) -group rust-hooks rust-indent-on-closing-paren
    hook window InsertChar \) -group rust-hooks rust-indent-on-closing-square
    hook window NormalKey <a-J> -group rust-hooks rust-remove-comment-markers
    hook window BufWritePre %val{buffile} -group rust-hooks rust-remove-trailing-whitespace
    hook -once -always window WinSetOption filetype=.* %{ remove-hooks window rust-.+ }
]
