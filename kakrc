colorscheme milo

# Indent = 4 spaces
set-option global tabstop 4
set-option global indentwidth 4

# No mouse mode, please...
set-option -add global ui_options ncurses_enable_mouse=false

# Hell yeah
set-option -add global ui_options ncurses_assistant=dilbert

# Wrap at 79 columns
set-option global autowrap_column 79

# Highlight column 80
hook global WinCreate .* %{
    add-highlighter window/hlcol column 80 default,rgb:aa0000
}

# More shortcuts
map global normal <c-e> 'vj' -docstring 'scroll down'
map global normal <c-y> 'vk' -docstring 'scroll up'

# Shortcut to change autowrap column
define-command toggle-autowrap-column -hidden %{ evaluate-commands %sh{
    if [ "$kak_count" -ne 0 ]; then
        count="$kak_count"
    elif [ "$kak_opt_autowrap_column" = 79 ]; then
        count=72
    else
        count=79
    fi
    oldmargin=$(echo "$kak_opt_autowrap_column + 1" | bc)
    margin=$(echo "$count + 1" | bc)
    echo "set-option window autowrap_column $count\n"
    echo "remove-highlighter window/hlcol"
    echo "add-highlighter window/hlcol column $margin default,rgb:aa0000"
} }
map global user c ':toggle-autowrap-column<ret>' \
    -docstring 'toggle autowrap column'

# Shortcut to set tabstop
define-command toggle-tabstop -hidden %{ evaluate-commands %sh{
    if [ ! "$kak_count" = 0 ]; then
        count="$kak_count"
    elif [ "$kak_opt_tabstop" = 4 ]; then
        count=8
    else
        count=4
    fi
    echo "set-option buffer tabstop $count\n"
    echo "echo 'tabstop is $count'"
} }
map global user t ':toggle-tabstop<ret>' \
    -docstring 'toggle tabstop'

# Autowrap lines
hook global WinCreate .* %{ autowrap-enable }

# Load .editorconfig files
hook global BufOpenFile .* %{ editorconfig-load }
hook global BufNewFile .* %{ editorconfig-load }

# Expand tabs to spaces on insert
hook global InsertChar \t %{ exec -draft 'h@' }
# Automatically erase a tab worth of spaces (sadly not aligned)
hook global InsertDelete ' ' %{ try %{
    execute-keys -itersel -draft 'h<a-h> <a-k>\A\h+\z<ret> i<space><esc><lt>'
} }

# Line numbers
define-command toggle-line-numbers -hidden %{ evaluate-commands %{
    try %{
        add-highlighter global/number-lines number-lines -relative -hlcursor
    } catch %{
        remove-highlighter global/number-lines
    }
} }
evaluate-commands toggle-line-numbers
map global user r ':toggle-line-numbers<ret>' \
    -docstring 'toggle line numbers'

# Line wrapping
add-highlighter global/ wrap
# Show matching parens
add-highlighter global/ show-matching

# Compile TeX files with LaTeX.
# 
# Sadly, I can't find a way to restrict this binding to LaTeX files.
# 
# It would be nice to pipe output to a new buffer, but it's surprisingly
# tricky to do. I just watch the log file with the less command.
define-command render-latex -hidden %{ evaluate-commands %sh{
    cd $(dirname "$kak_buffile")
    pdflatex "$kak_buffile" 2>&1 > "$kak_buffile.log"
} }

map global user l ':render-latex<ret>' \
    -docstring 'render TeX to PDF'

hook global BufOpenFile .* editorconfig-load
hook global BufNewFile .* editorconfig-load

# TODO: The new autowrap plugin broke my hooks. It needs replacing.
