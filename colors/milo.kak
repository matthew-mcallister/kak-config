# Milo (based on the Vim colorscheme Slate)
evaluate-commands %sh{
    white='rgb:ffffff'
    black='rgb:000000'
    light='rgb:aaaaaa'
    dark='rgb:555555'
    yellow='rgb:ffff55'
    brown='rgb:aa5500'
    pink='rgb:ff5555'
    red='rgb:aa0000'
    blue='rgb:5555ff'
    teal='rgb:00aaaa'
    green='rgb:00aa00'

    echo "
        # code
        face global value              ${brown}
        face global type               ${green}
        face global variable           ${brown}
        face global module             ${teal}
        face global function           ${brown}
        face global string             ${teal}
        face global keyword            ${blue}
        face global operator           ${pink}
        face global attribute          ${green}
        face global comment            ${yellow}
        face global meta               ${pink}
        face global builtin            ${white}

        # markup
        face global title              ${yellow}+b
        face global header             ${yellow}+b
        face global bold               ${white}+b
        face global italic             ${white}+i
        face global mono               ${pink}
        face global block              ${white}+i
        face global link               ${pink}
        face global bullet             ${brown}
        face global list               ${white}

        # builtin
        face global Default            ${white},${black}
        face global PrimarySelection   ${black},${brown}
        face global SecondarySelection ${black},${blue}
        face global PrimaryCursor      ${black},${green}
        face global SecondaryCursor    ${black},${light}
        face global PrimaryCursorEol   ${black},${green}
        face global SecondaryCursorEol ${black},${light}
        face global LineNumbers        ${brown},${black}
        face global LineNumberCursor   ${black},${brown}
        face global LineNumbersWrapped ${black},${black}
        face global MenuForeground     ${light},${black}
        face global MenuBackground     ${dark},${black}
        face global MenuInfo           ${brown},${black}
        face global Information        ${black},${brown}
        face global Error              ${black},${red}+b
        face global StatusLine         ${black},${white}+b
        face global StatusLineMode     ${black},${white}
        face global StatusLineInfo     ${black},${white}
        face global StatusLineValue    ${black},${white}
        face global StatusCursor       ${black},${green}
        face global Prompt             ${brown},${black}+b
        face global MatchingChar       ${black},${yellow}
        face global BufferPadding      ${blue},${black}
    "
}
